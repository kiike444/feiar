# FEI AR: Sistema de Realidad Aumentada de la Facultad de Estadística e Informática #

Aplicación móvil que consulta y muestra en realidad aumentada la información que recibe de la aplicación web del sistema FEI AR.

* Versión 1.0

### Dependencias ###

* Vuforia 3.0

### Requerimientos de los dispositivos ###

* Android 2.3.3 o superior
* Procesador ARMv7