package mx.uv.feiar.controller;

import java.util.ArrayList;

import mx.uv.feiar.model.Aviso;
import mx.uv.feiar.model.DbFeiArDataSource;
import mx.uv.feiar.model.DbFeiArHelper;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class AvisoController {
	private SQLiteDatabase db;
	
	public  AvisoController(Context context){		
		DbFeiArHelper dbHelper = DbFeiArHelper.getInstance(context);
		this.db = dbHelper.getWritableDatabase();
	}

	public boolean guardar(Aviso aviso){
		ContentValues registro = new ContentValues();
		registro.put(DbFeiArDataSource.CamposAviso.NOMBRE, aviso.getNombre());
		registro.put(DbFeiArDataSource.CamposAviso.DESCRIPCION, aviso.getDescripcion());
		registro.put(DbFeiArDataSource.CamposAviso.FECHA_INICIO, aviso.getFechaInicio());
		registro.put(DbFeiArDataSource.CamposAviso.FECHA_FIN, aviso.getFechaFin());
		registro.put(DbFeiArDataSource.CamposAviso.LUGAR, aviso.getLugar());
		
		long result = db.insert(DbFeiArDataSource.AVISO, null, registro);
		if(result > -1){
			return true;
		}else{
			return false;
		}
	}	
	
	public void eliminarDatosAviso(){
		db.delete(DbFeiArDataSource.AVISO, null, null);
	}
	
	public ArrayList<Aviso> getTodosLosAvisos(){
		String query = "SELECT * FROM " + DbFeiArDataSource.AVISO;
		Cursor c = db.rawQuery(query, null);
		ArrayList<Aviso> avisos = new ArrayList<Aviso>();
		if(c.moveToFirst()){
			do{
				String nombre = c.getString(0);
				String descripcion = c.getString(1);
				String fechaInicio = c.getString(2);
				String fechaFin = c.getString(3);
				String lugar = c.getString(4);
				avisos.add( new Aviso(nombre, descripcion, fechaInicio, fechaFin, lugar) );
			}while(c.moveToNext());
		}
		return avisos;
	}
	
	public void cerrarConexion(){
		db.close();
	}	
	
}
