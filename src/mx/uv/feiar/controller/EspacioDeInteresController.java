package mx.uv.feiar.controller;

import java.util.ArrayList;

import mx.uv.feiar.model.DbFeiArDataSource;
import mx.uv.feiar.model.DbFeiArHelper;
import mx.uv.feiar.model.EspacioDeInteres;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class EspacioDeInteresController {
	private SQLiteDatabase db;
	
	public EspacioDeInteresController (Context context){
		DbFeiArHelper dbHelper = DbFeiArHelper.getInstance(context);
		this.db = dbHelper.getWritableDatabase();		
	}
	
	public boolean guardar(EspacioDeInteres espacio){
		ContentValues registro = new ContentValues();
		registro.put(DbFeiArDataSource.CamposEspacioDeInteres.ID_ESPACIO_DE_INTERES, espacio.getIdEspacioDeInteres());
		registro.put(DbFeiArDataSource.CamposEspacioDeInteres.NOMBRE, espacio.getNombre());
		registro.put(DbFeiArDataSource.CamposEspacioDeInteres.DESCRIPCION, espacio.getDescripcion());
		registro.put(DbFeiArDataSource.CamposEspacioDeInteres.URL_SITIO_WEB, espacio.getUrlSitioWeb());
		registro.put(DbFeiArDataSource.CamposEspacioDeInteres.ID_MARCADOR, espacio.getIdMarcador());
		
		long result = db.insert(DbFeiArDataSource.ESPACIO_DE_INTERES, null, registro);
		if(result > -1){
			return true;
		}else{
			return false;
		}
	}
	
	public void eliminarDatosEspacioDeInteres(){
		db.delete(DbFeiArDataSource.ESPACIO_DE_INTERES, null, null);
	}	
	
	
	public ArrayList<Integer> getIdMarcadoresDeEspacios(){
		String query = "SELECT " + DbFeiArDataSource.CamposEspacioDeInteres.ID_MARCADOR +" FROM " + DbFeiArDataSource.ESPACIO_DE_INTERES;
		Cursor c = db.rawQuery(query, null);
		ArrayList<Integer> idsMarcadores = new ArrayList<Integer>();
		if (c.moveToFirst()) {
		     do {
		    	 idsMarcadores.add(Integer.valueOf(c.getInt(0)));
		     } while(c.moveToNext());
		}		
		
		c.close();
		return idsMarcadores;
	}	
	
	public EspacioDeInteres getEspacioDeInteresPorIdMarcador(int idMarcador){
		String query = "SELECT * FROM " + DbFeiArDataSource.ESPACIO_DE_INTERES + " WHERE idMarcador = ?";
		String[] args = new String[]{Integer.toString(idMarcador)};
		Cursor c = db.rawQuery(query, args);
		EspacioDeInteres espacio = null;
		if (c.moveToFirst()) {
		     do {
		    	 int idEspacio = c.getInt(0);
		    	 String nombre = c.getString(1);
		    	 String descripcion = c.getString(2);
		    	 String url = c.getString(3);
		    	 int marcador = c.getInt(4);
		    	 espacio = new EspacioDeInteres(idEspacio, nombre, descripcion, url, marcador);
		     } while(c.moveToNext());
		}				
		c.close();
		return espacio;
	}
	
	public String getNombreEspacioDeInteresPorId(int idEspacio){
		String query = "SELECT nombre FROM " + DbFeiArDataSource.ESPACIO_DE_INTERES + " WHERE idEspacioDeInteres = ?";
		String[] args = new String[]{Integer.toString(idEspacio)};
		Cursor c = db.rawQuery(query, args);
		String nombre = "";
		if (c.moveToFirst()) {
			nombre = c.getString(0);
		}
		c.close();
		return nombre;
	}
	
	public void cerrarConexion(){
		db.close();
	}	
	
}
