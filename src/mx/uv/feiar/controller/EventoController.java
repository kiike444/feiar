package mx.uv.feiar.controller;

import java.util.ArrayList;

import mx.uv.feiar.model.DbFeiArDataSource;
import mx.uv.feiar.model.DbFeiArHelper;
import mx.uv.feiar.model.Evento;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class EventoController {
	private SQLiteDatabase db;
	
	public EventoController(Context context){		
		DbFeiArHelper dbHelper = DbFeiArHelper.getInstance(context);
		this.db = dbHelper.getWritableDatabase();
	}
	
	public boolean guardar(Evento evento){
		ContentValues registro = new ContentValues();
		registro.put(DbFeiArDataSource.CamposEvento.ID_EVENTO, evento.getIdEvento());
		registro.put(DbFeiArDataSource.CamposEvento.NOMBRE, evento.getNombre());
		registro.put(DbFeiArDataSource.CamposEvento.DESCRIPCION, evento.getDescripcion());
		registro.put(DbFeiArDataSource.CamposEvento.FECHA_INICIO, evento.getFechaInicio());
		registro.put(DbFeiArDataSource.CamposEvento.FECHA_FIN, evento.getFechaFin());
		registro.put(DbFeiArDataSource.CamposEvento.URL_SITIO_WEB, evento.getUrlSitioWeb());
		registro.put(DbFeiArDataSource.CamposEvento.TIPO, evento.getTipo());
		registro.put(DbFeiArDataSource.CamposEvento.ID_ESPACIO_DE_INTERES, evento.getIdEspacioDeInteres());
		registro.put(DbFeiArDataSource.CamposEvento.LUGAR, evento.getLugar());
		registro.put(DbFeiArDataSource.CamposEvento.ID_MARCADOR, evento.getIdMarcador());
		
		long result = db.insert(DbFeiArDataSource.EVENTO, null, registro);
		if(result > -1){
			return true;
		}else{
			return false;
		}
	}	
	
	public int eliminarDatosEventoInterno(){
		String where = DbFeiArDataSource.CamposEvento.ID_MARCADOR + " = ?";
		String[] whereArgs = { "-1" };		
		int result = db.delete(DbFeiArDataSource.EVENTO, where, whereArgs);
		return result;
	}		
	
	public int eliminarDatosEventoExterno(){
		String where = DbFeiArDataSource.CamposEvento.ID_ESPACIO_DE_INTERES + " = ?";
		String[] whereArgs = { "0" };		
		int result = db.delete(DbFeiArDataSource.EVENTO, where, whereArgs);
		return result;
	}	
	
	public ArrayList<Integer> getIdMarcadoresDeEventosExternos(){
		String query = "SELECT " + DbFeiArDataSource.CamposEvento.ID_MARCADOR +" FROM " + DbFeiArDataSource.EVENTO + " WHERE idMarcador > -1";
		Cursor c = db.rawQuery(query, null);
		ArrayList<Integer> idsMarcadores = new ArrayList<Integer>();
		if (c.moveToFirst()) {
		     do {
		    	 idsMarcadores.add(Integer.valueOf(c.getInt(0)));
		     } while(c.moveToNext());
		}		
		c.close();
		return idsMarcadores;
	}		
	
	public Evento getEventoExternoPorIdMarcador(int idMarcador){
		String query = "SELECT * FROM " + DbFeiArDataSource.EVENTO + " WHERE idMarcador = ?";
		String[] args = new String[]{Integer.toString(idMarcador)};
		Cursor c = db.rawQuery(query, args);
		Evento evento = null;
		if (c.moveToFirst()) {
		     do {
		    	 int idEvento = c.getInt(0);
		    	 String nombre = c.getString(1);
		    	 String descripcion = c.getString(2);
		    	 String fechaInicio = c.getString(3);
		    	 String fechaFin = c.getString(4);
		    	 String urlSitioWeb = c.getString(5);
		    	 String tipo = c.getString(6);
		    	 int idEspacioDeInteres = c.getInt(7);
		    	 String lugar = c.getString(8);
		    	 int marcador = c.getInt(9);
		    	 evento = new Evento (idEvento, nombre, descripcion, fechaInicio, fechaFin, urlSitioWeb, tipo, idEspacioDeInteres, lugar, marcador);
		     } while(c.moveToNext());
		}				
		c.close();
		return evento;
	}	

	public ArrayList<Evento> getEventosInternosDeEspacioDeInteres(int idEspacioDeInteres){
		String query = "SELECT * FROM " + DbFeiArDataSource.EVENTO + " WHERE idEspacioDeInteres = ?";
		String[] args = new String[]{Integer.toString(idEspacioDeInteres)};
		Cursor c = db.rawQuery(query, args);
		ArrayList<Evento> listaEventos = new ArrayList<Evento>();
		if(c.moveToFirst()){
			do{
		    	 int idEvento = c.getInt(0);
		    	 String nombre = c.getString(1);
		    	 String descripcion = c.getString(2);
		    	 String fechaInicio = c.getString(3);
		    	 String fechaFin = c.getString(4);
		    	 String urlSitioWeb = c.getString(5);
		    	 String tipo = c.getString(6);
		    	 int idEspacio = c.getInt(7);
		    	 String lugar = c.getString(8);
		    	 int marcador = c.getInt(9);	
		    	 listaEventos.add(new Evento (idEvento, nombre, descripcion, fechaInicio, fechaFin, urlSitioWeb, tipo, idEspacio, lugar, marcador));
			}while(c.moveToNext());
		}
		return listaEventos;
	}
	
	public ArrayList<Evento> getTodosLosEventos(){
		String query = "SELECT * FROM " + DbFeiArDataSource.EVENTO;
		Cursor c = db.rawQuery(query, null);
		ArrayList<Evento> listaEventos = new ArrayList<Evento>();
		if(c.moveToFirst()){
			do{
		    	 int idEvento = c.getInt(0);
		    	 String nombre = c.getString(1);
		    	 String descripcion = c.getString(2);
		    	 String fechaInicio = c.getString(3);
		    	 String fechaFin = c.getString(4);
		    	 String urlSitioWeb = c.getString(5);
		    	 String tipo = c.getString(6);
		    	 int idEspacio = c.getInt(7);
		    	 String lugar = c.getString(8);
		    	 int marcador = c.getInt(9);	
		    	 listaEventos.add(new Evento (idEvento, nombre, descripcion, fechaInicio, fechaFin, urlSitioWeb, tipo, idEspacio, lugar, marcador));
			}while(c.moveToNext());
		}
		return listaEventos;
	}
	
	public void cerrarConexion(){
		db.close();
	}	
	

}
