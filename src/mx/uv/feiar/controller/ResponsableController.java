package mx.uv.feiar.controller;

import mx.uv.feiar.model.DbFeiArDataSource;
import mx.uv.feiar.model.DbFeiArHelper;
import mx.uv.feiar.model.Responsable;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class ResponsableController {

	private SQLiteDatabase db;
	
	public ResponsableController(Context context){		
		DbFeiArHelper dbHelper = DbFeiArHelper.getInstance(context);
		this.db = dbHelper.getWritableDatabase();
	}	
	
	public boolean guardar(Responsable responsable){
		ContentValues registro = new ContentValues();
		registro.put(DbFeiArDataSource.CamposResponsable.ID_RESPONSABLE, responsable.getIdResponsable());
		registro.put(DbFeiArDataSource.CamposResponsable.NOMBRE, responsable.getNombre());
		registro.put(DbFeiArDataSource.CamposResponsable.CORREO, responsable.getCorreoInstitucional());
		
		long result = db.insert(DbFeiArDataSource.RESPONSABLE, null, registro);
		if(result > -1){
			return true;
		}else{
			return false;
		}
	}	
	
	public String getNombreDeResponsable(int idResponsable){
		String query = "SELECT " + DbFeiArDataSource.CamposResponsable.NOMBRE + " FROM " + DbFeiArDataSource.RESPONSABLE + " WHERE idResponsable = ?";
		String[] args = new String[]{Integer.toString(idResponsable)};
		Cursor c = db.rawQuery(query, args);
		String nombre = "";
		if(c.moveToFirst()){
			nombre = c.getString(0);
		}
		
		return nombre;
	}
	
	public void eliminarDatosResponsable(){
		db.delete(DbFeiArDataSource.RESPONSABLE, null, null);
	}		
	
	public void cerrarConexion(){
		db.close();
	}	
	
}
