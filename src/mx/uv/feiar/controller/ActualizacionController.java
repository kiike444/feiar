package mx.uv.feiar.controller;

import mx.uv.feiar.model.Actualizacion;
import mx.uv.feiar.model.DbFeiArDataSource;
import mx.uv.feiar.model.DbFeiArHelper;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class ActualizacionController {
	private SQLiteDatabase db;
	
	public ActualizacionController(Context context){		
		DbFeiArHelper dbHelper = DbFeiArHelper.getInstance(context);
		this.db = dbHelper.getWritableDatabase();
	}

	public boolean guardar(Actualizacion actualizacion){
		ContentValues registro = new ContentValues();
		registro.put(DbFeiArDataSource.CamposActualizacion.DEPENDENCIA, actualizacion.getDependencia());
		registro.put(DbFeiArDataSource.CamposActualizacion.ESPACIOS, actualizacion.getEspaciosDeInteres());
		registro.put(DbFeiArDataSource.CamposActualizacion.SERVICIOS, actualizacion.getServicios());
		registro.put(DbFeiArDataSource.CamposActualizacion.EVENTOS, actualizacion.getEventos());
		registro.put(DbFeiArDataSource.CamposActualizacion.SENIALAMIENTOS, actualizacion.getSenialamientos());
		registro.put(DbFeiArDataSource.CamposActualizacion.RESPONSABLES, actualizacion.getResponsables());
		registro.put(DbFeiArDataSource.CamposActualizacion.AVISOS, actualizacion.getAvisos());
		
		long result = db.insert(DbFeiArDataSource.ACTUALIZACION, null, registro);
		if(result > -1){
			return true;
		}else{
			return false;
		}
	}	
	
	public void eliminarDatosActualizacion(){
		db.delete(DbFeiArDataSource.ACTUALIZACION, null, null);
		
	}
	
	public boolean estaActualizadoDependencia(String fechaDependencia){
		String query = "SELECT * FROM " + DbFeiArDataSource.ACTUALIZACION + " WHERE "+ DbFeiArDataSource.CamposActualizacion.DEPENDENCIA +" = ?";
		Cursor c = db.rawQuery(query, new String[]{fechaDependencia});
		
		if (c.moveToFirst()) {
			c.close();
			
		     return true;
		}else{
			c.close();
			
			return false;
		}		
	}	
	
	public boolean estaActualizadoEspacioDeInteres(String fechaEspacios){
		String query = "SELECT * FROM " + DbFeiArDataSource.ACTUALIZACION + " WHERE "+ DbFeiArDataSource.CamposActualizacion.ESPACIOS +" = ?";
		Cursor c = db.rawQuery(query, new String[]{fechaEspacios});
		
		if (c.moveToFirst()) {
			c.close();
			
		     return true;
		}else{
			c.close();
			
			return false;
		}		
	}
	
	public boolean estaActualizadoServicios(String fechaServicios){
		String query = "SELECT * FROM " + DbFeiArDataSource.ACTUALIZACION + " WHERE "+ DbFeiArDataSource.CamposActualizacion.SERVICIOS +" = ?";
		Cursor c = db.rawQuery(query, new String[]{fechaServicios});
		
		if (c.moveToFirst()) {
			c.close();
			
		     return true;
		}else{
			c.close();
			
			return false;
		}		
	}	
	
	public boolean estaActualizadoEventos(String fechaEventos){
		String query = "SELECT * FROM " + DbFeiArDataSource.ACTUALIZACION + " WHERE "+ DbFeiArDataSource.CamposActualizacion.EVENTOS +" = ?";
		Cursor c = db.rawQuery(query, new String[]{fechaEventos});
		
		if (c.moveToFirst()) {
			c.close();
			
		     return true;
		}else{
			c.close();
			
			return false;
		}		
	}		

	public boolean estaActualizadoSenialamientos(String fechaSenialamiento){
		String query = "SELECT * FROM " + DbFeiArDataSource.ACTUALIZACION + " WHERE "+ DbFeiArDataSource.CamposActualizacion.SENIALAMIENTOS +" = ?";
		Cursor c = db.rawQuery(query, new String[]{fechaSenialamiento});
		
		if (c.moveToFirst()) {
			c.close();
			
		     return true;
		}else{
			c.close();
			
			return false;
		}		
	}		
	
	public boolean estaActualizadoResponsable(String fechaResponsable){
		String query = "SELECT * FROM " + DbFeiArDataSource.ACTUALIZACION + " WHERE "+ DbFeiArDataSource.CamposActualizacion.RESPONSABLES +" = ?";
		Cursor c = db.rawQuery(query, new String[]{fechaResponsable});
		
		if (c.moveToFirst()) {
			c.close();
			
		     return true;
		}else{
			c.close();
			
			return false;
		}		
	}		
	
	public boolean estaActualizadoAviso(String fechaAviso){
		String query = "SELECT * FROM " + DbFeiArDataSource.ACTUALIZACION + " WHERE "+ DbFeiArDataSource.CamposActualizacion.AVISOS +" = ?";
		Cursor c = db.rawQuery(query, new String[]{fechaAviso});
		
		if (c.moveToFirst()) {
			c.close();
			
		     return true;
		}else{
			c.close();
			
			return false;
		}		
	}			
	
	public void cerrarConexion(){
		db.close();
	}
	
}
