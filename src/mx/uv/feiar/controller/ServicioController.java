package mx.uv.feiar.controller;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import mx.uv.feiar.model.DbFeiArDataSource;
import mx.uv.feiar.model.DbFeiArHelper;
import mx.uv.feiar.model.Servicio;

public class ServicioController {
	private SQLiteDatabase db;
	
	public ServicioController(Context context){
		DbFeiArHelper dbHelper = DbFeiArHelper.getInstance(context);
		this.db = dbHelper.getWritableDatabase();
	}
	
	public boolean guardar(Servicio servicio){
		ContentValues registro = new ContentValues();
		registro.put(DbFeiArDataSource.CamposServicio.ID_SERVICIO, servicio.getIdServicio());
		registro.put(DbFeiArDataSource.CamposServicio.NOMBRE, servicio.getNombre());
		registro.put(DbFeiArDataSource.CamposServicio.DESCRIPCION, servicio.getDescripcion());
		registro.put(DbFeiArDataSource.CamposServicio.ID_ESPACIO_DE_INTERES, servicio.getIdEspacioDeInteres());
		registro.put(DbFeiArDataSource.CamposServicio.ID_RESPONSABLE, servicio.getIdResponsable());
		
		long result = db.insert(DbFeiArDataSource.SERVICIO, null, registro);
		if(result > -1){
			return true;
		}else{
			return false;
		}
	}	
	
	public void eliminarDatosServicio(){
		db.delete(DbFeiArDataSource.SERVICIO, null, null);
	}	
	
	public ArrayList<Servicio> getServiciosDeEspacioDeInteres(int idEspacioDeInteres){
		String query = "SELECT * FROM " + DbFeiArDataSource.SERVICIO + " WHERE idEspacioDeInteres = ?";
		String[] args = new String[]{Integer.toString(idEspacioDeInteres)};
		Cursor c = db.rawQuery(query, args);	
		ArrayList<Servicio> listaServicios = new ArrayList<Servicio>();
		if(c.moveToFirst()){
			do{
				int idServicio = c.getInt(0);
				String nombre = c.getString(1);
				String descripcion = c.getString(2);
				int idEspacio = c.getInt(3);
				int idResponsable = c.getInt(4);
				listaServicios.add(new Servicio(idServicio, nombre, descripcion, idEspacio, idResponsable));
			}while(c.moveToNext());
		}
		return listaServicios;
	}
	

	public void cerrarConexion(){
		db.close();
	}	
	
}
