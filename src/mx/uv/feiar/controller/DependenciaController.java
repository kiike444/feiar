package mx.uv.feiar.controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import mx.uv.feiar.model.DbFeiArDataSource;
import mx.uv.feiar.model.DbFeiArHelper;
import mx.uv.feiar.model.Dependencia;

public class DependenciaController {
	private SQLiteDatabase db;
	
	public DependenciaController(Context context){		
		DbFeiArHelper dbHelper = DbFeiArHelper.getInstance(context);
		this.db = dbHelper.getWritableDatabase();
	}
	
	public boolean guardar(Dependencia dependencia){
		ContentValues registro = new ContentValues();
		registro.put(DbFeiArDataSource.CamposDependencia.ID_DEPENDENCIA, dependencia.getIdDependencia());
		registro.put(DbFeiArDataSource.CamposDependencia.NOMBRE, dependencia.getNombre());
		registro.put(DbFeiArDataSource.CamposDependencia.URL_SERVIDOR_AVISOS, dependencia.getUrlServidorAvisos());
		
		long result = db.insert(DbFeiArDataSource.DEPENDENCIA, null, registro);
		if(result > -1){
			return true;
		}else{
			return false;
		}
	}
	
	public int getIdDependenciaAsociada(){
		int idDependencia = 0;	
		String query = "SELECT idDependencia FROM " + DbFeiArDataSource.DEPENDENCIA;
		Cursor c = db.rawQuery(query, null);
		
		if (c.moveToFirst()) {
		     do {
		    	 idDependencia = c.getInt(0);
		     } while(c.moveToNext());
		}		
		
		c.close();
		return idDependencia;
	}
	
	public String getNombreDependenciaAsoaciada(){
		String query = "SELECT nombre FROM " + DbFeiArDataSource.DEPENDENCIA;
		Cursor c = db.rawQuery(query, null);
		String nombre = "";
		if (c.moveToFirst()) {
		     do {
		    	 nombre = c.getString(0);
		     } while(c.moveToNext());
		}		
		
		c.close();
		return nombre;		
	}

	public String getUrlServidorAvisosDependenciaAsoaciada(){
		String query = "SELECT urlServidorAvisos FROM " + DbFeiArDataSource.DEPENDENCIA;
		Cursor c = db.rawQuery(query, null);
		String url = "";
		if (c.moveToFirst()) {
		     do {
		    	 url = c.getString(0);
		     } while(c.moveToNext());
		}		
		
		c.close();
		return url;		
	}	
	
	public void eliminarDatosDependencia(){
		db.delete(DbFeiArDataSource.DEPENDENCIA, null, null);
	}
	
	public void cerrarConexion(){
		db.close();
	}
	
}
