package mx.uv.feiar.controller;

import java.util.ArrayList;

import mx.uv.feiar.model.DbFeiArDataSource;
import mx.uv.feiar.model.DbFeiArHelper;
import mx.uv.feiar.model.Indicacion;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class IndicacionController {
	private SQLiteDatabase db;
	
	public IndicacionController(Context context){		
		DbFeiArHelper dbHelper = DbFeiArHelper.getInstance(context);
		this.db = dbHelper.getWritableDatabase();
	}	
	
	public boolean guardar(Indicacion indicacion){
		ContentValues registro = new ContentValues();
		registro.put(DbFeiArDataSource.CamposIndicacion.ID_INDICACION, indicacion.getIdIndicacion());
		registro.put(DbFeiArDataSource.CamposIndicacion.DESCRIPCION, indicacion.getDescripcion());
		registro.put(DbFeiArDataSource.CamposIndicacion.ORIENTACION, indicacion.getOrientacion());
		registro.put(DbFeiArDataSource.CamposIndicacion.ID_ESPACIO_DE_INTERES , indicacion.getIdEspacioDeInteres());
		
		long result = db.insert(DbFeiArDataSource.INDICACION, null, registro);
		if(result > -1){
			return true;
		}else{
			return false;
		}
	}	
	
	public void eliminarDatosIndicacion(){
		db.delete(DbFeiArDataSource.INDICACION, null, null);
	}		
	
	public ArrayList<Indicacion> getIndicacionesDeEspacioDeInteres(int idEspacio){
		String query = "SELECT * FROM " + DbFeiArDataSource.INDICACION + " WHERE idEspacioDeInteres = ?";
		String[] args = new String[]{Integer.toString(idEspacio)};
		Cursor c = db.rawQuery(query, args);
		ArrayList<Indicacion> listaIndicaciones = new ArrayList<Indicacion>();
		if(c.moveToFirst()){
			do{
				int idIndicacion = c.getInt(0);
				String descripcion = c.getString(1);
				String orientacion = c.getString(2);
				int idEspacioDeInteres = c.getInt(3);
				listaIndicaciones.add(new Indicacion(idIndicacion,descripcion,orientacion,idEspacioDeInteres));
			}while(c.moveToNext());
		}
		
		return listaIndicaciones;
	}
	
	public void cerrarConexion(){
		db.close();
	}	
	
}
