package mx.uv.feiar.model;

public class Evento {
	private int idEvento;
	private String nombre;
	private String descripcion;		
	private String fechaInicio;
	private String fechaFin;
	private String urlSitioWeb;
	private String tipo;
	private int idEspacioDeInteres;
	private String lugar;
	private int idMarcador;
	
	public Evento(int idEvento, String nombre, String descripcion, String fechaInicio, String fechaFin, String urlSitioWeb, String tipo, int idEspacioDeInteres, String lugar, int idMarcador){
		this.idEvento = idEvento;
		this.nombre = nombre;
		this.descripcion = descripcion;	
		this.fechaInicio = fechaInicio;
		this.fechaFin = fechaFin;
		this.urlSitioWeb = urlSitioWeb;
		this.tipo = tipo;
		this.idEspacioDeInteres = idEspacioDeInteres;
		this.lugar = lugar;
		this.idMarcador = idMarcador;
	}

	public int getIdEvento() {
		return idEvento;
	}

	public void setIdEvento(int idEvento) {
		this.idEvento = idEvento;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}

	public String getUrlSitioWeb() {
		return urlSitioWeb;
	}

	public void setUrlSitioWeb(String urlSitioWeb) {
		this.urlSitioWeb = urlSitioWeb;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public int getIdEspacioDeInteres() {
		return idEspacioDeInteres;
	}

	public void setIdEspacioDeInteres(int idEspacioDeInteres) {
		this.idEspacioDeInteres = idEspacioDeInteres;
	}

	public String getLugar() {
		return lugar;
	}

	public void setLugar(String lugar) {
		this.lugar = lugar;
	}

	public int getIdMarcador() {
		return idMarcador;
	}

	public void setIdMarcador(int idMarcador) {
		this.idMarcador = idMarcador;
	}
	
	
	
}
