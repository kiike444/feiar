package mx.uv.feiar.model;

public class EspacioDeInteres {
	private int idEspacioDeInteres;
	private String nombre;
	private String descripcion;
	private String urlSitioWeb;
	private int idMarcador;
	
	public EspacioDeInteres(int idEspacioDeInteres, String nombre, String descripcion, String urlSitioWeb, int idMarcador){
		this.idEspacioDeInteres = idEspacioDeInteres;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.urlSitioWeb = urlSitioWeb;
		this.idMarcador = idMarcador;
	}
	

	public int getIdEspacioDeInteres() {
		return idEspacioDeInteres;
	}

	public void setIdEspacioDeInteres(int idEspacioDeInteres) {
		this.idEspacioDeInteres = idEspacioDeInteres;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getUrlSitioWeb() {
		return urlSitioWeb;
	}

	public void setUrlSitioWeb(String urlSitioWeb) {
		this.urlSitioWeb = urlSitioWeb;
	}

	public int getIdMarcador() {
		return idMarcador;
	}

	public void setIdMarcador(int idMarcador) {
		this.idMarcador = idMarcador;
	}
	
	
	
}
