package mx.uv.feiar.model;

public class Actualizacion {
	private String dependencia;
	private String espaciosDeInteres;			
	private String servicios;
	private String eventos;
	private String senialamientos;
	private String responsables;
	private String avisos = "";
	
	public Actualizacion(String dependencia, String espaciosDeInteres, String servicios, String eventos, String senialamientos, String responsables){
		this.dependencia = dependencia;
		this.espaciosDeInteres = espaciosDeInteres;
		this.servicios = servicios;
		this.eventos = eventos;
		this.senialamientos = senialamientos;
		this.responsables = responsables;
	}

	public String getDependencia() {
		return dependencia;
	}

	public void setDependencia(String dependencia) {
		this.dependencia = dependencia;
	}

	public String getEspaciosDeInteres() {
		return espaciosDeInteres;
	}

	public void setEspaciosDeInteres(String espaciosDeInteres) {
		this.espaciosDeInteres = espaciosDeInteres;
	}

	public String getServicios() {
		return servicios;
	}

	public void setServicios(String servicios) {
		this.servicios = servicios;
	}

	public String getEventos() {
		return eventos;
	}

	public void setEventos(String eventos) {
		this.eventos = eventos;
	}

	public String getSenialamientos() {
		return senialamientos;
	}

	public void setSenialamientos(String senialamientos) {
		this.senialamientos = senialamientos;
	}

	public String getResponsables() {
		return responsables;
	}

	public void setResponsables(String responsables) {
		this.responsables = responsables;
	}

	public String getAvisos() {
		return avisos;
	}

	public void setAvisos(String avisos) {
		this.avisos = avisos;
	}	
	
	
}
