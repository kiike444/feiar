package mx.uv.feiar.model;

public class Indicacion {
	private int idIndicacion;
	private String descripcion;			
	private String orientacion;
	private int idEspacioDeInteres;
	
	public Indicacion(int idIndicacion, String descripcion, String orientacion, int idEspacioDeInteres){
		this.idIndicacion = idIndicacion;
		this.descripcion = descripcion;
		this.orientacion = orientacion;
		this.idEspacioDeInteres = idEspacioDeInteres;
	}

	public int getIdIndicacion() {
		return idIndicacion;
	}

	public void setIdIndicacion(int idIndicacion) {
		this.idIndicacion = idIndicacion;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getOrientacion() {
		return orientacion;
	}

	public void setOrientacion(String orientacion) {
		this.orientacion = orientacion;
	}

	public int getIdEspacioDeInteres() {
		return idEspacioDeInteres;
	}

	public void setIdEspacioDeInteres(int idEspacioDeInteres) {
		this.idEspacioDeInteres = idEspacioDeInteres;
	}
	
	
	
}
