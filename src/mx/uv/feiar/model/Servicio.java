package mx.uv.feiar.model;

public class Servicio {
	private int idServicio;
	private String nombre;
	private String descripcion;
	private int idEspacioDeInteres;
	private int idResponsable;
	
	public Servicio(int idServicio, String nombre, String descripcion, int idEspacioDeInteres, int idResponsable){
		this.idServicio = idServicio;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.idEspacioDeInteres = idEspacioDeInteres;
		this.idResponsable = idResponsable;
	}

	public int getIdServicio() {
		return idServicio;
	}

	public void setIdServicio(int idServicio) {
		this.idServicio = idServicio;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getIdEspacioDeInteres() {
		return idEspacioDeInteres;
	}

	public void setIdEspacioDeInteres(int idEspacioDeInteres) {
		this.idEspacioDeInteres = idEspacioDeInteres;
	}

	public int getIdResponsable() {
		return idResponsable;
	}

	public void setIdResponsable(int idResponsable) {
		this.idResponsable = idResponsable;
	}
	
	
	
}
