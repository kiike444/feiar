package mx.uv.feiar.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbFeiArHelper extends SQLiteOpenHelper{

	public static final String DATABASE_NAME = "DBFeiar.db";
    public static final int DATABASE_VERSION = 1;	
    private static DbFeiArHelper instanciaDB = null;
	
	public DbFeiArHelper(Context context){
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	public static DbFeiArHelper getInstance(Context ctx) {
		if (instanciaDB == null) {
			instanciaDB = new DbFeiArHelper(ctx.getApplicationContext());
		}
		return instanciaDB;
	}	
	
	public void onCreate(SQLiteDatabase db){
		//Crear tablas
		db.execSQL(DbFeiArDataSource.CREATE_DEPENDENCIA_SCRIPT);
		db.execSQL(DbFeiArDataSource.CREATE_ESPACIO_DE_INTERES_SCRIPT);
		db.execSQL(DbFeiArDataSource.CREATE_SERVICIO_SCRIPT);
		db.execSQL(DbFeiArDataSource.CREATE_RESPONSABLE_SCRIPT);
		db.execSQL(DbFeiArDataSource.CREATE_EVENTO_SCRIPT);
		db.execSQL(DbFeiArDataSource.CREATE_INDICACION_SCRIPT);
		db.execSQL(DbFeiArDataSource.CREATE_ACTUALIZACION_SCRIPT);
	}
	
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
		//A�ade los cambios que se realizar�n en el esquema
	}
	
}
