package mx.uv.feiar.model;

public class DbFeiArDataSource {

	public static final String DEPENDENCIA = "Dependencia";
	public static final String ESPACIO_DE_INTERES = "EspacioDeInteres";
	public static final String SERVICIO = "Servicio";
	public static final String RESPONSABLE = "Responsable";
	public static final String EVENTO = "Evento";
	public static final String INDICACION = "Indicacion";
	public static final String ACTUALIZACION = "Actualizacion";
	public static final String AVISO = "Aviso";
	
	public static final String STRING_TYPE = "TEXT";
	public static final String INT_TYPE = "INT";
	
	//Campos de la tabla Dependencia
	public static class CamposDependencia{
		public static final String ID_DEPENDENCIA = "idDependencia";
		public static final String NOMBRE = "nombre";
		public static final String COORDENADAS = "coordenadas";
		public static final String URL_SERVIDOR_AVISOS = "urlServidorAvisos";
	}
	
	public static final String CREATE_DEPENDENCIA_SCRIPT = "CREATE TABLE " + DEPENDENCIA + "(" +
		CamposDependencia.ID_DEPENDENCIA + " " + INT_TYPE + " PRIMARY KEY," +
		CamposDependencia.NOMBRE + " " + STRING_TYPE + " NOT NULL," +
		CamposDependencia.URL_SERVIDOR_AVISOS + " " + STRING_TYPE + ")";
	
	//Campos de la tabla EspacioDeInteres
	public static class CamposEspacioDeInteres{
		public static final String ID_ESPACIO_DE_INTERES = "idEspacioDeInteres";
		public static final String NOMBRE = "nombre";
		public static final String DESCRIPCION = "descripcion";
		public static final String URL_SITIO_WEB = "urlSitioWeb";
		public static final String ID_MARCADOR = "idMarcador";
	}
	
	public static final String CREATE_ESPACIO_DE_INTERES_SCRIPT = "CREATE TABLE " + ESPACIO_DE_INTERES + "(" +
			CamposEspacioDeInteres.ID_ESPACIO_DE_INTERES + " " + INT_TYPE + " PRIMARY KEY," +
			CamposEspacioDeInteres.NOMBRE + " " + STRING_TYPE + " NOT NULL," +
			CamposEspacioDeInteres.DESCRIPCION + " " + STRING_TYPE + " NOT NULL," +
			CamposEspacioDeInteres.URL_SITIO_WEB + " " + STRING_TYPE + ", " +
			CamposEspacioDeInteres.ID_MARCADOR + " " + INT_TYPE + ")";	
	
	//Campos de la tabla Servicios
	public static class CamposServicio{
		public static final String ID_SERVICIO = "idServicio";
		public static final String NOMBRE = "nombre";
		public static final String DESCRIPCION = "descripcion";
		public static final String ID_ESPACIO_DE_INTERES = "idEspacioDeInteres";
		public static final String ID_RESPONSABLE = "idResponsable";
	}

	public static final String CREATE_SERVICIO_SCRIPT = "CREATE TABLE " + SERVICIO + "(" +
			CamposServicio.ID_SERVICIO + " " + INT_TYPE + " PRIMARY KEY," +
			CamposServicio.NOMBRE + " " + STRING_TYPE + " NOT NULL," +
			CamposServicio.DESCRIPCION + " " + STRING_TYPE + " NOT NULL," +
			CamposServicio.ID_ESPACIO_DE_INTERES + " " + INT_TYPE + " NOT NULL, " +
			CamposServicio.ID_RESPONSABLE + " " + INT_TYPE + " NOT NULL)";		

	//Campos de la tabla Responsable
	public static class CamposResponsable{
		public static final String ID_RESPONSABLE = "idResponsable";
		public static final String NOMBRE = "nombre";
		public static final String CORREO = "correoInstitucional";
	}	

	public static final String CREATE_RESPONSABLE_SCRIPT = "CREATE TABLE " + RESPONSABLE + "(" +
			CamposResponsable.ID_RESPONSABLE + " " + INT_TYPE + " PRIMARY KEY," +
			CamposResponsable.NOMBRE + " " + STRING_TYPE + " NOT NULL," +
			CamposResponsable.CORREO + " " + STRING_TYPE + " NOT NULL)";		
	
	//Campos de la tabla Evento
	public static class CamposEvento{
		public static final String ID_EVENTO = "idEvento";
		public static final String NOMBRE = "nombre";
		public static final String DESCRIPCION = "descripcion";		
		public static final String FECHA_INICIO = "fechaInicio";
		public static final String FECHA_FIN = "fechaFin";
		public static final String URL_SITIO_WEB = "urlSitioWeb";
		public static final String TIPO = "tipo";
		public static final String ID_ESPACIO_DE_INTERES = "idEspacioDeInteres";
		public static final String LUGAR = "lugar";
		public static final String ID_MARCADOR= "idMarcador";
	}

	public static final String CREATE_EVENTO_SCRIPT = "CREATE TABLE " + EVENTO + "(" +
			CamposEvento.ID_EVENTO + " " + INT_TYPE + " PRIMARY KEY," +
			CamposEvento.NOMBRE + " " + STRING_TYPE + " NOT NULL," +
			CamposEvento.DESCRIPCION + " " + STRING_TYPE + " NOT NULL," +
			CamposEvento.FECHA_INICIO + " " + STRING_TYPE + " NOT NULL," +
			CamposEvento.FECHA_FIN + " " + STRING_TYPE + " NOT NULL," +
			CamposEvento.URL_SITIO_WEB + " " + STRING_TYPE + " NOT NULL," +
			CamposEvento.TIPO+ " " + STRING_TYPE + " NOT NULL," +
			CamposEvento.ID_ESPACIO_DE_INTERES + " " + INT_TYPE + " NOT NULL, " +
			CamposEvento.LUGAR + " " + STRING_TYPE + ", " +
			CamposEvento.ID_MARCADOR + " " + INT_TYPE + ")";			
	
	//Campos de la tabla Indicacion
	public static class CamposIndicacion{
		public static final String ID_INDICACION = "idIndicacion";
		public static final String DESCRIPCION = "descripcion";			
		public static final String ORIENTACION = "orientacion";
		public static final String ID_ESPACIO_DE_INTERES = "idEspacioDeInteres";
	}	

	public static final String CREATE_INDICACION_SCRIPT = "CREATE TABLE " + INDICACION + "(" +
			CamposIndicacion.ID_INDICACION + " " + INT_TYPE + " PRIMARY KEY," +
			CamposIndicacion.DESCRIPCION + " " + STRING_TYPE + " NOT NULL," +
			CamposIndicacion.ORIENTACION + " " + STRING_TYPE + " NOT NULL," +
			CamposIndicacion.ID_ESPACIO_DE_INTERES + " " + STRING_TYPE + " NOT NULL)";	

	//Campos de la tabla Actualizacion
	public static class CamposActualizacion{
		public static final String DEPENDENCIA = "dependencia";
		public static final String ESPACIOS = "espaciosDeInteres";			
		public static final String SERVICIOS = "servicios";
		public static final String EVENTOS = "eventos";
		public static final String SENIALAMIENTOS = "senialamientos";
		public static final String RESPONSABLES = "responsables";
		public static final String AVISOS = "avisos";
	}	

	public static final String CREATE_ACTUALIZACION_SCRIPT = "CREATE TABLE " + ACTUALIZACION + "(" +
			CamposActualizacion.DEPENDENCIA + " " + STRING_TYPE + " NOT NULL," +
			CamposActualizacion.ESPACIOS + " " + STRING_TYPE + " NOT NULL," +
			CamposActualizacion.SERVICIOS + " " + STRING_TYPE + " NOT NULL," +
			CamposActualizacion.EVENTOS + " " + STRING_TYPE + " NOT NULL," +
			CamposActualizacion.SENIALAMIENTOS + " " + STRING_TYPE + " NOT NULL," +
			CamposActualizacion.RESPONSABLES + " " + STRING_TYPE + " NOT NULL," +
			CamposActualizacion.AVISOS + " " + STRING_TYPE + ")";	
	
	//Campos de la tabla Aviso
	public static class CamposAviso{
		public static final String NOMBRE = "nombre";
		public static final String DESCRIPCION = "descripcion";
		public static final String FECHA_INICIO = "fechaInicio";
		public static final String FECHA_FIN = "fechaFin";
		public static final String LUGAR = "lugar";
	}	

	public static final String CREATE_AVISO_SCRIPT = "CREATE TABLE " + AVISO + "(" +
			CamposAviso.NOMBRE + " " + STRING_TYPE + " NOT NULL," +		
			CamposAviso.DESCRIPCION + " " + STRING_TYPE + " NOT NULL," +
			CamposAviso.FECHA_INICIO + " " + STRING_TYPE + " NOT NULL," +
			CamposAviso.FECHA_FIN + " " + STRING_TYPE + " NOT NULL," +
			CamposAviso.LUGAR + " " + STRING_TYPE + ")";		

	
	public DbFeiArDataSource(){
	
	}
	
}
