package mx.uv.feiar.model;

public class Responsable {
	private int idResponsable;
	private String nombre;
	private String correoInstitucional;
	
	public Responsable(int idResponsable, String nombre, String correoInstitucional){
		this.idResponsable = idResponsable;
		this.nombre = nombre;
		this.correoInstitucional = correoInstitucional;
	}

	public int getIdResponsable() {
		return idResponsable;
	}

	public void setIdResponsable(int idResponsable) {
		this.idResponsable = idResponsable;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCorreoInstitucional() {
		return correoInstitucional;
	}

	public void setCorreoInstitucional(String correoInstitucional) {
		this.correoInstitucional = correoInstitucional;
	}
	
	
	
}
