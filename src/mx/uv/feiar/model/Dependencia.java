package mx.uv.feiar.model;

public class Dependencia {
	private int idDependencia;
	private String nombre;
	private String urlServidorAvisos;
	
	public Dependencia(int idDependencia, String nombre, String coordenadas, String urlServidorAvisos){
		this.idDependencia = idDependencia;
		this.nombre = nombre;
		this.urlServidorAvisos = urlServidorAvisos;
	}

	public int getIdDependencia() {
		return idDependencia;
	}

	public void setIdDependencia(int idDependencia) {
		this.idDependencia = idDependencia;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getUrlServidorAvisos() {
		return urlServidorAvisos;
	}

	public void setUrlServidorAvisos(String urlServidorAvisos) {
		this.urlServidorAvisos = urlServidorAvisos;
	}
	
}
