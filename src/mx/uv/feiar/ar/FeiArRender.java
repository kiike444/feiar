package mx.uv.feiar.ar;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import com.qualcomm.vuforia.Renderer;
import com.qualcomm.vuforia.State;
import com.qualcomm.vuforia.Tool;
import com.qualcomm.vuforia.TrackableResult;
import com.qualcomm.vuforia.Vuforia;
import com.qualcomm.vuforia.samples.SampleApplication.SampleApplicationSession;
import com.qualcomm.vuforia.samples.SampleApplication.utils.SampleUtils;
import com.qualcomm.vuforia.samples.SampleApplication.utils.Texture;

import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;

public class FeiArRender implements GLSurfaceView.Renderer {

    boolean deleteCurrentProductTexture = false;
    private Texture mProductTexture;    
    SampleApplicationSession vuforiaAppSession;
    // Referencia a la activity principal
    public AugmentedReality mActivity;    
    private float mScaleFactor;
    private float mDPIScaleIndicator;
    boolean texturaCreada = false;
    boolean texturaGeneradaOPENGL = false;
    public boolean mIsActive = false;
    private int shaderProgramID;
    private int vertexHandle;
    private int normalHandle;
    private int textureCoordHandle;
    private int mvpMatrixHandle;    
    private Plane mPlane;
    private int mScreenHeight;
    private int mScreenWidth;
    private float[] modelViewMatrix;

   
    // Texture is Generated and Target is Acquired - Rendering Data
    public static final int RS_NORMAL = 0;
    
    // Target has been lost - Rendering transition to 2D Overlay
    public static final int RS_TRANSITION_TO_2D = 1;
    
    // Target has been required - Rendering transition to 3D
    public static final int RS_TRANSITION_TO_3D = 2;
    
    // New Target has been found - Loading data and generating OpenGL
    // Textures
    public static final int RS_LOADING = 3;
    
    // Texture has been generated in Java - Ready to be generated
    // in OpenGL in the renderFrame thread
    public static final int RS_TEXTURE_GENERATED = 4;
    
    // AuugmentedReality is active and scanning - Searching for targets.
    public static final int RS_SCANNING = 5;    
    // Initialize RenderState
    int renderState = RS_SCANNING;     
    
    
    public FeiArRender(AugmentedReality activity, SampleApplicationSession session){
            mActivity = activity;
            vuforiaAppSession = session;
    }    
	
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        // Call function to initialize rendering:
        initRendering();
        // Call Vuforia function to (re)initialize rendering after first use
        // or after OpenGL ES context was lost (e.g. after onPause/onResume):
        Vuforia.onSurfaceCreated();
	}

	@Override
	public void onSurfaceChanged(GL10 gl, int width, int height) {
        // Call Vuforia function to handle render surface size changes:
        mScreenHeight = height;
        mScreenWidth = width;
        // Call function to update rendering when render surfaceparameters have changed:
        updateRendering(width, height);
        // Call Vuforia function to handle render surface size changes:
        Vuforia.onSurfaceChanged(width, height);
	}

	@Override
	public void onDrawFrame(GL10 gl) {
        if (!mIsActive)
            return;
        // Call our function to render content
        renderFrame();
	}
	
    // The render function - Funci�n que renderiza la textura creada en el Thread principal de la Activity
    public void renderFrame(){
        // Clear color and depth buffer
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
        
        // Get the state from Vuforia and mark the beginning of a rendering section
        State state = Renderer.getInstance().begin();
        
        // Explicitly render the Video Background
        Renderer.getInstance().drawVideoBackground();
        

        GLES20.glEnable(GLES20.GL_DEPTH_TEST);
        GLES20.glEnable(GLES20.GL_CULL_FACE);
        
        if (deleteCurrentProductTexture){
            // Elimina la textura actual para cambiarla por otra m�s nueva creada en el Thread princial de la Activity
            if (mProductTexture != null){
                GLES20.glDeleteTextures(1, mProductTexture.mTextureID, 0);
                mProductTexture = null;
            }
            
            deleteCurrentProductTexture = false;
        }
        
        // If the render state indicates that the texture is generated it generates the OpenGL texture for start drawing the plane
        if (renderState == RS_TEXTURE_GENERATED){
            generateProductTextureInOpenGL();
        }      	
        
        //Se pregunta si se encontraron marcadores en el Frame
        if(state.getNumTrackableResults() > 0){
            // Get the trackable:
            TrackableResult trackableResult = state.getTrackableResult(0);
            if (trackableResult == null){
                return;
            }
            
            modelViewMatrix = Tool.convertPose2GLMatrix(trackableResult.getPose()).getData();
            // Si la textura creada en el Thread principal no es NULL se dibuja la textura en la c�mara
            if(mProductTexture != null){
            	renderAugmentation(trackableResult);
            }
            
        }

        GLES20.glDisable(GLES20.GL_DEPTH_TEST);
        
        Renderer.getInstance().end();
    }	
    
    /*
     * Genera la textura en OpenGL para aplicarla a un Plano
     */
    private void generateProductTextureInOpenGL(){
        Texture textureObject = mActivity.getProductTexture();
        
        if (textureObject != null){
            mProductTexture = textureObject;
        }
        
        // Generates the Texture in OpenGL
        GLES20.glGenTextures(1, mProductTexture.mTextureID, 0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mProductTexture.mTextureID[0]);
        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
        
        // Create an empty power of two texture and upload a sub image.
        GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA, 1024,
            1024, 0, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, null);
        
        GLES20.glTexSubImage2D(GLES20.GL_TEXTURE_2D, 0, 0, 0,
            mProductTexture.mWidth, mProductTexture.mHeight, GLES20.GL_RGBA,
            GLES20.GL_UNSIGNED_BYTE, mProductTexture.mData);
        
        // Updates the current Render State
        renderState = RS_NORMAL;
    }   
    
  
    
    private void renderAugmentation(TrackableResult trackableResult){
        float[] modelViewProjection = new float[16];
        
        // Scales the plane relative to the target
        Matrix.scaleM(modelViewMatrix, 0, 430.f * mScaleFactor,430.f * mScaleFactor, 1.0f);
        
        // Applies 3d Transformations to the plane
        Matrix.multiplyMM(modelViewProjection, 0, vuforiaAppSession.getProjectionMatrix().getData(), 0, modelViewMatrix, 0);
        
        // Shader Program for drawing
        GLES20.glUseProgram(shaderProgramID);
        
        // El plano solo se dibuja cuando la textura esta cargada y generada
        if (renderState == RS_NORMAL)
        {
            GLES20.glVertexAttribPointer(vertexHandle, 3, GLES20.GL_FLOAT, false, 0, mPlane.getVertices());
            GLES20.glVertexAttribPointer(normalHandle, 3, GLES20.GL_FLOAT, false, 0, mPlane.getNormals());
            GLES20.glVertexAttribPointer(textureCoordHandle, 2, GLES20.GL_FLOAT, false, 0, mPlane.getTexCoords());
            
            GLES20.glEnableVertexAttribArray(vertexHandle);
            GLES20.glEnableVertexAttribArray(normalHandle);
            GLES20.glEnableVertexAttribArray(textureCoordHandle);
            
            // Enables Blending State - �IMPORTARNTE! Aplica transparencia a las Texturas
            GLES20.glEnable(GLES20.GL_BLEND);
            GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
            
            // Drawing Textured Plane - Se dibuja el plano en la c�mara
            GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mProductTexture.mTextureID[0]);
            GLES20.glUniformMatrix4fv(mvpMatrixHandle, 1, false, modelViewProjection, 0);
            GLES20.glDrawElements(GLES20.GL_TRIANGLES, 6, GLES20.GL_UNSIGNED_SHORT, mPlane.getIndices());
            
            GLES20.glDisableVertexAttribArray(vertexHandle);
            GLES20.glDisableVertexAttribArray(normalHandle);
            GLES20.glDisableVertexAttribArray(textureCoordHandle);
            
            // Disables Blending State - Its important to disable the blending
            // state after using it for preventing bugs with the Camera Video
            // Background
            GLES20.glDisable(GLES20.GL_BLEND);
            
        }
        
    }    
	
    // Function for initializing the renderer.
    public void initRendering(){
        
        // Define clear color
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, Vuforia.requiresAlpha() ? 0.0f
            : 1.0f);
        
        // OpenGL setup for 3D model
        shaderProgramID = SampleUtils.createProgramFromShaderSrc(
            Shaders.cubeMeshVertexShader, Shaders.cubeFragmentShader);
        
        vertexHandle = GLES20.glGetAttribLocation(shaderProgramID,
            "vertexPosition");
        normalHandle = GLES20.glGetAttribLocation(shaderProgramID,
            "vertexNormal");
        textureCoordHandle = GLES20.glGetAttribLocation(shaderProgramID,
            "vertexTexCoord");
        mvpMatrixHandle = GLES20.glGetUniformLocation(shaderProgramID,
            "modelViewProjectionMatrix");
        
        mPlane = new Plane();
  
    }	
	
    // Function to update the renderer.
    public void updateRendering(int width, int height){
        // Update screen dimensions
        mScreenWidth = width;
        mScreenHeight = height;
    }        
	
    public void deleteCurrentProductTexture(){
        deleteCurrentProductTexture = true;
    }	
    
    public void setProductTexture(Texture texture){
        mProductTexture = texture;
        
    }   
    
    public void setDPIScaleIndicator(float dpiSIndicator){
        mDPIScaleIndicator = dpiSIndicator;
        
    }    
    
    public void setScaleFactor(float f){
        mScaleFactor = f;
        
    }    
    
    public void setRenderState(int state){
        renderState = state;
        
    }
    
    
    public int getRenderState(){
        return renderState;
    }

}
