package mx.uv.feiar.ar;

import mx.uv.feiar.R;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class OverlayViewSenialamiento extends RelativeLayout{
	
	private int numIndicaciones = 1;
	
    public OverlayViewSenialamiento(Context context){
        this(context, null);
    }
    
    public OverlayViewSenialamiento(Context context, AttributeSet attrs){
        this(context, attrs, 0);
    }    
    
    public OverlayViewSenialamiento(Context context, AttributeSet attrs, int defStyle){
        super(context, attrs, defStyle);
        inflateLayout(context);
    }    
    
    private void inflateLayout(Context context){
        final LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.bitmap_layout_senialamiento, this, true);
    }    
    
    public void setIndicacion(String orientacion, String descripcion){
    	TextView tv = null;
    	ImageView iv = null;
    	switch (numIndicaciones) {
			case 1:
				iv = (ImageView) findViewById(R.id.ivOrientacion1);
				tv = (TextView) findViewById(R.id.tvDescripcion1);
				break;
			case 2:
				iv = (ImageView) findViewById(R.id.ivOrientacion2);
				tv = (TextView) findViewById(R.id.tvDescripcion2);
				break;
			case 3:
				iv = (ImageView) findViewById(R.id.ivOrientacion3);
				tv = (TextView) findViewById(R.id.tvDescripcion3);
				break;
			case 4:
				iv = (ImageView) findViewById(R.id.ivOrientacion4);
				tv = (TextView) findViewById(R.id.tvDescripcion4);
				break;
			case 5:
				iv = (ImageView) findViewById(R.id.ivOrientacion5);
				tv = (TextView) findViewById(R.id.tvDescripcion5);
				break;			
			case 6:
				iv = (ImageView) findViewById(R.id.ivOrientacion6);
				tv = (TextView) findViewById(R.id.tvDescripcion6);
				break;	
			case 7:
				iv = (ImageView) findViewById(R.id.ivOrientacion7);
				tv = (TextView) findViewById(R.id.tvDescripcion7);
				break;
			case 8:
				iv = (ImageView) findViewById(R.id.ivOrientacion8);
				tv = (TextView) findViewById(R.id.tvDescripcion8);
				break;			
		}
    	
    	switch (orientacion) {
			case "arriba":
				iv.setImageResource(R.drawable.ic_arriba);
				break;
			case "arriba-derecha":
				iv.setImageResource(R.drawable.ic_arriba_derecha);
				break;
			case "derecha":
				iv.setImageResource(R.drawable.ic_derecha);
				break;
			case "abajo-derecha":
				iv.setImageResource(R.drawable.ic_abajo_derecha);
				break;
			case "abajo":
				iv.setImageResource(R.drawable.ic_abajo);
				break;
			case "abajo-izquierda":
				iv.setImageResource(R.drawable.ic_abajo_izquierda);
				break;
			case "izquierda":
				iv.setImageResource(R.drawable.ic_izquierda);
				break;
			case "arriba-izquierda":
				iv.setImageResource(R.drawable.ic_arriba_izquierda);
				break;			
		}
    	
    	tv.setText(descripcion);
    	numIndicaciones++;
    }
    
}
