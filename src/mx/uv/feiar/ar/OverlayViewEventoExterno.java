package mx.uv.feiar.ar;

import mx.uv.feiar.R;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class OverlayViewEventoExterno extends RelativeLayout{

    public OverlayViewEventoExterno(Context context){
        this(context, null);
    }
    
    public OverlayViewEventoExterno(Context context, AttributeSet attrs){
        this(context, attrs, 0);
    }    
    
    public OverlayViewEventoExterno(Context context, AttributeSet attrs, int defStyle){
        super(context, attrs, defStyle);
        inflateLayout(context);
    }    
    
    private void inflateLayout(Context context){
        final LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.bitmap_layout_evento_externo, this, true);
    }      
    
    public void setNombre(String nombre){
        TextView tv = (TextView) findViewById(R.id.tvEventoExternoNombre);
        tv.setText(nombre);
    }    
    
    public void setDescripcion(String descripcion){
        TextView tv = (TextView) findViewById(R.id.tvEventoExternoDescripcion);
        tv.setText(descripcion);
    }
    
    public void setLugar(String lugar){
        TextView tv = (TextView) findViewById(R.id.tvEventoExternoLugar);
        tv.setText(lugar);
    }        

    public void setFechaInicio(String fechaInicio){
		String fechaIni = fechaInicio.split(" ")[0];
		String horaInicio = fechaInicio.split(" ")[1];
		horaInicio = horaInicio.substring(0, 5);	
		
        TextView tv = (TextView) findViewById(R.id.tvEventoExternoFechaInicio);
        tv.setText("El "+ fechaIni + " a las " + horaInicio);
    }        
    
    public void setFechaFin(String fechaFin){
		String fechaF = fechaFin.split(" ")[0];
		String horaFin = fechaFin.split(" ")[1];
		horaFin = horaFin.substring(0, 5);
    	
        TextView tv = (TextView) findViewById(R.id.tvEventoExternoFechaFin);
        tv.setText("El "+ fechaF + " a las " + horaFin);
    }            
}
