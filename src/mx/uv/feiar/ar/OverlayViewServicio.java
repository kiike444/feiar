package mx.uv.feiar.ar;

import mx.uv.feiar.R;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class OverlayViewServicio extends RelativeLayout{

    public OverlayViewServicio(Context context){
        this(context, null);
    }
    
    public OverlayViewServicio(Context context, AttributeSet attrs){
        this(context, attrs, 0);
    }    
    
    public OverlayViewServicio(Context context, AttributeSet attrs, int defStyle){
        super(context, attrs, defStyle);
        inflateLayout(context);
    }    
    
    private void inflateLayout(Context context){
        final LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.bitmap_layout_servicio, this, true);
    }     
    
    public void setNombre(String nombre){
        TextView tv = (TextView) findViewById(R.id.tvNombreServicio);
        tv.setText(nombre);
    }    
    
    public void setDescripcion(String descripcion){
        TextView tv = (TextView) findViewById(R.id.tvServicioDescripcion);
        tv.setText(descripcion);
    }    
    
    public void setResponsable(String responsable){
        TextView tv = (TextView) findViewById(R.id.tvResponsableServicio);
        tv.setText(responsable);
    }        
    
    public void setServicioActual(int actual){
        TextView tv = (TextView) findViewById(R.id.tvServicioActual);
        tv.setText(""+actual);
    }            
	
    public void setTotalServicios(int total){
        TextView tv = (TextView) findViewById(R.id.tvTotalServicios);
        tv.setText(""+total);
    }      
    
}
