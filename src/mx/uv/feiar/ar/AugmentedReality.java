package mx.uv.feiar.ar;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import mx.uv.feiar.R;
import mx.uv.feiar.controller.EspacioDeInteresController;
import mx.uv.feiar.controller.EventoController;
import mx.uv.feiar.controller.IndicacionController;
import mx.uv.feiar.controller.ResponsableController;
import mx.uv.feiar.controller.ServicioController;
import mx.uv.feiar.model.EspacioDeInteres;
import mx.uv.feiar.model.Evento;
import mx.uv.feiar.model.Indicacion;
import mx.uv.feiar.model.Servicio;

import com.qualcomm.vuforia.CameraDevice;
import com.qualcomm.vuforia.Marker;
import com.qualcomm.vuforia.MarkerResult;
import com.qualcomm.vuforia.MarkerTracker;
import com.qualcomm.vuforia.State;
import com.qualcomm.vuforia.TrackableResult;
import com.qualcomm.vuforia.Tracker;
import com.qualcomm.vuforia.TrackerManager;
import com.qualcomm.vuforia.Vec2F;
import com.qualcomm.vuforia.Vuforia;
import com.qualcomm.vuforia.samples.SampleApplication.SampleApplicationControl;
import com.qualcomm.vuforia.samples.SampleApplication.SampleApplicationException;
import com.qualcomm.vuforia.samples.SampleApplication.SampleApplicationSession;
import com.qualcomm.vuforia.samples.SampleApplication.utils.SampleApplicationGLView;
import com.qualcomm.vuforia.samples.SampleApplication.utils.Texture;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class AugmentedReality extends Activity implements SampleApplicationControl{
	private String urlEspacio = "";
	private String urlEventoExterno = "";
	private String urlEventoInterno = "";
	private String accionActual = "";
	private int indiceTextura = 0;
	private int ultimoIndiceTextura = -1;
	private int ultimoIdMarcadorEncontrado = -1;
	Texture texturaPrincipal = null;
	private ArrayList<Evento> listaEventos = new ArrayList<Evento>();
	private ArrayList<Servicio> listaServicios = new ArrayList<Servicio>();
	private ArrayList<String> listaNombresResponsables = new ArrayList<String>();
	private ArrayList<Indicacion> listaIndicaciones = new ArrayList<Indicacion>();
	
	//--------------------------------------------------------------------------------
	//                             Variables de Vuforia 3
	//--------------------------------------------------------------------------------
	
    // Our OpenGL view:
    private SampleApplicationGLView mGlView;   
    // Our renderer:
    private FeiArRender mRenderer;
    //Our frame markers:
    private Marker dataSet[];
    // size of the Texture to be generated with the element data
    private static int mTextureSize = 768; 	
    boolean mIsDroidDevice = false;
    //Scale factor of texture from screen
    private float mdpiScaleIndicator;
    //Tag for the Log
    private static final String LOGTAG = "FEI-AR";
    SampleApplicationSession vuforiaAppSession;
    
	//--------------------------------------------------------------------------------
	//                Variables de la interfaz de usuario
	//--------------------------------------------------------------------------------
    
    private RelativeLayout mUILayout;
    private TextView mStatusBar;
    private ImageView ivDescripcion;
    private ImageView ivServicios;
    private ImageView ivEventos;
    private ImageView ivSenialamiento;
    private ImageView ivSitioWeb;
    private ImageView ivAnterior;
    private ImageView ivSiguiente;
    private LinearLayout menu;
    private LinearLayout menuDesplazamiento;

    // Handles Codes to display/Hide views
    static final int HIDE_STATUS_BAR = 0;
    static final int SHOW_STATUS_BAR = 1; 
    static final int HIDE_2D_OVERLAY = 0;
    static final int SHOW_2D_OVERLAY = 1;
    static final int HIDE_LOADING_DIALOG = 0;
    static final int SHOW_LOADING_DIALOG = 1;    
    private LoadingDialogHandler loadingDialogHandler = new LoadingDialogHandler(this);    
    
    
    //----------------------------------------------------------------------
    // 	                    Funciones de la actividad
    //----------------------------------------------------------------------
    
    //Llamado cuando la actividad es llamada por primera vez. Despu�s de esta llamada siempre se llama a onStart()
    @Override
    protected void onCreate(Bundle savedInstanceState){
        Log.d(LOGTAG, "onCreate");
        super.onCreate(savedInstanceState);
        vuforiaAppSession = new SampleApplicationSession(this);
        vuforiaAppSession.initAR(this, ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
        mdpiScaleIndicator = getApplicationContext().getResources().getDisplayMetrics().density;
        mIsDroidDevice = android.os.Build.MODEL.toLowerCase().startsWith("droid");   
        
        //Durante la animaci�n de carga se crean los eventos del men� de la interfaz de usuario
        startLoadingAnimation();
    }    
	
    /*
     * Llamada cuando la actividad va a empezar a interactuar con el usuario, en este punto es 
     * el �ltimo punto antes de que el usuario ya vea la actividad y pueda empezar a interactuar 
     * con ella. Siempre despues de un onResume() viene un onPause().
     */
    @Override
    protected void onResume(){
        Log.d(LOGTAG, "onResume");
        super.onResume();
        
        if (mIsDroidDevice){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
        
        try{
            vuforiaAppSession.resumeAR();
        } catch (SampleApplicationException e){
            Log.e(LOGTAG, e.getString());
        }
        
        // Resume the GL view:
        if (mGlView != null){
            mGlView.setVisibility(View.VISIBLE);
            mGlView.onResume();
        }        
        
    }	
    
    @Override
    public void onConfigurationChanged(Configuration config)
    {
        Log.d(LOGTAG, "onConfigurationChanged");
        super.onConfigurationChanged(config);
        
        vuforiaAppSession.onConfigurationChanged();
    }
    
    /*
     * LLamada cuando el sistema va a empezar una nueva actividad. �sta necesita parar 
     * animaciones, y parar todo lo que est� haciendo. Despu�s de esta llamada puede 
     * venir un onResume() si la actividad vuelve a primer plano o un onStop() si se 
     * hace invisible para el usuario.
     */
    @Override
    protected void onPause(){
        Log.d(LOGTAG, "onPause");
        super.onPause();
        
        try{
            vuforiaAppSession.pauseAR();
        } catch (SampleApplicationException e){
            Log.e(LOGTAG, e.getString());
        }
        
        // When the camera stops it clears the Product Texture ID so next time textures are recreated
        if (mRenderer != null){
            mRenderer.deleteCurrentProductTexture();
            // Initialize all state Variables
            initStateVariables();
        }
        
        // Pauses the OpenGLView
        if (mGlView != null){
            mGlView.setVisibility(View.INVISIBLE);
            mGlView.onPause();
        }
        
        ultimoIdMarcadorEncontrado=-1;
    }    
    
    /*
     * Esta es la llamada final a la actividad, despu�s de �sta, es totalmente destruida.
     */
    @Override
    protected void onDestroy()
    {
        Log.d(LOGTAG, "onDestroy");
        super.onDestroy();
        
        try{
            vuforiaAppSession.stopAR();
        } catch (SampleApplicationException e){
            Log.e(LOGTAG, e.getString());
        }
        
        System.gc();
    }    
    
    
    
    //----------------------------------------------------------------------
    // 	                    Funciones del framework Vuforia 3
    //----------------------------------------------------------------------    
    
    
    	//---------------------------------------------------------------------------------------
        //	Las siguientes funciones est�n relacionadas con la creaci�n y configuraci�n
        //  del Tracker. (Lo que activa la realidad aumentada, en este caso los FrameMarkers)
    	//---------------------------------------------------------------------------------------
    
    /*
     * The following code initializes the Tracker - Lo que va a reconocer los marcadores
     */
	@Override
	public boolean doInitTrackers() {
        // Indicate if the trackers were initialized correctly
        boolean result = true;
        
        // Initialize the marker tracker:
        TrackerManager trackerManager = TrackerManager.getInstance();
        Tracker trackerBase = trackerManager.initTracker(MarkerTracker.getClassType());
        MarkerTracker markerTracker = (MarkerTracker) (trackerBase);
        
        if (markerTracker == null)
        {
            Log.e(
                LOGTAG,
                "Tracker not initialized. Tracker already initialized or the camera is already started");
            result = false;
        } else
        {
            Log.i(LOGTAG, "Tracker successfully initialized");
        }
        
        return result;
	}
	
	
    /*
     * This initializes the datasets - Creamos los marcadores que va a reconocer el Tracker
     */
	@Override
	public boolean doLoadTrackersData() {
        TrackerManager tManager = TrackerManager.getInstance();
        MarkerTracker markerTracker = (MarkerTracker) tManager.getTracker(MarkerTracker.getClassType());
        if (markerTracker == null)
            return false;
        
        EspacioDeInteresController espacioController = new EspacioDeInteresController(AugmentedReality.this);
        EventoController eventoController = new EventoController(AugmentedReality.this);
        //Se obtienen los IDs de los Marcadores usados por la dependencia (Espacios de inter�s y Eventos eternos)
        ArrayList<Integer> idMarcadoresEspacios = espacioController.getIdMarcadoresDeEspacios();
        ArrayList<Integer> idMarcadoresEventosExternos = eventoController.getIdMarcadoresDeEventosExternos();
        
        //Cada ID de marcador se registra en el Tracker para ser reconocido posteriormente
        dataSet = new Marker[idMarcadoresEspacios.size() + idMarcadoresEventosExternos.size()];
        
        int i = 0;
        for(Integer idMarcador : idMarcadoresEspacios){
        	String nombre = "espacio " + i;
        	dataSet[i] = markerTracker.createFrameMarker(idMarcador.intValue(), nombre, new Vec2F(50, 50));
            if (dataSet[i] == null)
            {
            	Log.e("FEIAR", "Failed to create frame marker of place.");
                return false;
            }        	
        	i++;
        }
        
        espacioController.cerrarConexion();

        for(Integer idMarcador : idMarcadoresEventosExternos){
        	String nombre = "evento " + i;
        	dataSet[i] = markerTracker.createFrameMarker(idMarcador.intValue(), nombre, new Vec2F(50, 50));
            if (dataSet[i] == null)
            {
            	Log.e("FEIAR", "Failed to create frame marker of event.");
                return false;
            }
        	i++;
        }        
        
        eventoController.cerrarConexion();

       Log.i("doLoadTrackersData", "Successfully initialized MarkerTracker.");
        
        return true;
	}
	
	/*
	 * This starts the actual Tracker - Inicia el Tracker que va a reconocer los marcadores
	 */
	@Override
	public boolean doStartTrackers() {
        // Indicate if the trackers were started correctly
        boolean result = true;
        
        TrackerManager tManager = TrackerManager.getInstance();
        MarkerTracker markerTracker = (MarkerTracker) tManager.getTracker(MarkerTracker.getClassType());
        if (markerTracker != null)
            markerTracker.start();
        
        return result;
	}
	
	/*
	 * This stops the actual Tracker - Detiene el Tracker que reconoce los marcadores
	 */	
	@Override
	public boolean doStopTrackers() {
        // Indicate if the trackers were stopped correctly
        boolean result = true;
        
        TrackerManager tManager = TrackerManager.getInstance();
        MarkerTracker markerTracker = (MarkerTracker) tManager
            .getTracker(MarkerTracker.getClassType());
        if (markerTracker != null)
            markerTracker.stop();
        
        return result;
	}
	
	/*
	 * This destroys the datasets
	 */		
	@Override
	public boolean doUnloadTrackersData() {
        // Indicate if the trackers were unloaded correctly
        boolean result = true;
        return result;
	}
	
	/*
	 * This de-initializes the Tracker
	 */		
	@Override
	public boolean doDeinitTrackers() {
        // Indicate if the trackers were deinitialized correctly
        boolean result = true;
        TrackerManager tManager = TrackerManager.getInstance();
        tManager.deinitTracker(MarkerTracker.getClassType());
        return result;
	}
	
	
		//---------------------------------------------------------------------------------------
	    //	The following two callback functions onInitARDone() - onQCARUpdate() are invoked 
	    //  asynchronously, depending on a change in state within Vuforia:
		//---------------------------------------------------------------------------------------	
	
	
	@Override
	public void onInitARDone(SampleApplicationException e) {
        if (e == null){
        	initApplicationAR();
        	
            // Now add the GL surface view. It is important
            // that the OpenGL ES surface view gets added
            // BEFORE the camera is started and video
            // background is configured.
            addContentView(mGlView, new LayoutParams(LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT));     
            
            // Start the camera:
            try
            {
                vuforiaAppSession.startAR(CameraDevice.CAMERA.CAMERA_DEFAULT);
            } catch (SampleApplicationException ex)
            {
                Log.e(LOGTAG, ex.getString());
            }
            
            mRenderer.mIsActive = true;
            
            boolean result = CameraDevice.getInstance().setFocusMode(
                CameraDevice.FOCUS_MODE.FOCUS_MODE_CONTINUOUSAUTO);
            
            if (!result)
                Log.e(LOGTAG, "Unable to enable continuous autofocus");
        	
            //Mostramos la camara y escondemos el dialogo de carga
            mUILayout.bringToFront();
            loadingDialogHandler.sendEmptyMessage(HIDE_LOADING_DIALOG);
            mUILayout.setBackgroundColor(Color.TRANSPARENT);            
            
        }else
        {
            Log.e(LOGTAG, e.getString());

        }
	}
	
    // Initializes AR application components.
    private void initApplicationAR()
    {
        // Create OpenGL ES view:
        int depthSize = 16;
        int stencilSize = 0;
        boolean translucent = Vuforia.requiresAlpha();
        
        // Initialize the GLView with proper flags
        mGlView = new SampleApplicationGLView(this);
        mGlView.init(translucent, depthSize, stencilSize);
        
        // Setups the Renderer of the GLView
        mRenderer = new FeiArRender(this, vuforiaAppSession);
        mGlView.setRenderer(mRenderer);        
        
        // Sets the device scale density
        setDeviceDPIScaleFactor(mdpiScaleIndicator);
        
        initStateVariables();
    }    	
	
    private void initStateVariables(){
        mRenderer.setProductTexture(null);
    }  	    
    
    /*
     * Funci�n que sirve para comunicar el Thread principal de la actividad y el Thread del Render,
     * aqui se pueden crean y cambiar las texturas en tiempo de ejecuci�n dependendiendo si el Tracker encontr�
     * un marcador o no.
     */
	@Override
	public void onQCARUpdate(State state) {
		//Se pregunta si el Tracker encontr� marcadores
		if(state.getNumTrackableResults() > 0){
			TrackableResult trackableResult = state.getTrackableResult(0);
            MarkerResult markerResult = (MarkerResult) (trackableResult);
            Marker marker = (Marker) markerResult.getTrackable();  
            boolean texturaCreada = false;
            
            //Se pregunta si el Marcador es diferente al �ltimo que fue encontrado
            if(ultimoIdMarcadorEncontrado != marker.getMarkerId()){
            	show2DOverlay();
            	mRenderer.deleteCurrentProductTexture();
                mRenderer.setRenderState(FeiArRender.RS_LOADING);
                
                //Se pregunta si el marcador esta relacionado a un espacio de inter�s
                if(marker.getName().indexOf("espacio") > -1){
                	//Se cargan los datos del espacio relacionado con el marcador
                	EspacioDeInteres espacio = buscarEspacioDeInteres(marker.getMarkerId());
                	//Se crea la textura que ser� dibujada con los datos del espacio de inter�s
                	crearTexturaEspacioDeInteres(espacio);
                	this.urlEspacio = espacio.getUrlSitioWeb();
                	this.urlEventoInterno = "";
                	this.urlEventoExterno = "";
                	this.accionActual = "render-espacio";
                	texturaCreada = true;
                	int idEspacio = espacio.getIdEspacioDeInteres();
                	//Se buscan los servicios, eventos internos y las indicaciones de se�alamiento del espacio
                	buscarServiciosDeEspacioDeInteres(idEspacio);
                	buscarEventosInternosDeEspacioDeInteres(idEspacio);
                	buscarIndicacionesDeEspacioDeInteres(idEspacio);
                	ultimoIndiceTextura = -1;
                }else{
                	//Se pregunta si el marcador esta relacionado a un evento externo
                	if(marker.getName().indexOf("evento") > -1){
                		//Se cargan los datos del evento relacionado con el marcador
                		Evento evento = buscarEventoExterno(marker.getMarkerId());
                		//Se crea la textura que ser� dibujada con los datos del evento externo
                		crearTexturaEventoExterno(evento);
                		this.urlEventoExterno = evento.getUrlSitioWeb();
                    	this.urlEventoInterno = "";
                    	this.urlEspacio = "";
                		this.accionActual = "render-evento-externo";
                		texturaCreada = true;
                		hide2DOverlay();
                	}
                }                  
                synchronized(this){
                	this.ultimoIdMarcadorEncontrado = marker.getMarkerId();   
                 }
                
            }else{
            	//Se pregunta si en el men� se hizo clic a la acci�n de ver servicios del espacio de inter�s
                if(this.accionActual.equals("render-servicio") && (ultimoIndiceTextura != indiceTextura)){
                	//Se borra la textura actual
                	mRenderer.deleteCurrentProductTexture();
                    mRenderer.setRenderState(FeiArRender.RS_LOADING);
                	ultimoIndiceTextura = indiceTextura;
                	//Se crea la textura del servicio de la lista de servicios del espacio indicado por el indiceTextura
        	    	//Se deja un tiempo de 800 milisegundos para dar tiempo al Thread del Render a que borre la textura actual
                	Handler handler = new Handler();
        	        handler.postDelayed(new Runnable() {
        	            public void run() {
        	            	//Se crea la textura del servicio
        	            	crearTexturaServicio(listaServicios.get(indiceTextura), listaNombresResponsables.get(indiceTextura), indiceTextura+1, listaServicios.size());
        	            }
        	        }, 800);
                	accionActual = "render-servicio";
                	texturaCreada = true;
                	//Se muestra el men� de desplazamiento (Los botones <- ->) para cambiar entre servicios
                	muestraMenuDesplazamiento();
                }
                
                //Se pregunta si en el men� se hizo clic a la acci�n de ver eventos internos del espacio de inter�s
                if(this.accionActual.equals("render-evento-interno") && (ultimoIndiceTextura != indiceTextura)){
                	//Se borra la textura actual
                	mRenderer.deleteCurrentProductTexture();
                    mRenderer.setRenderState(FeiArRender.RS_LOADING);
                    ultimoIndiceTextura = indiceTextura;
                    this.urlEventoInterno = listaEventos.get(indiceTextura).getUrlSitioWeb();
                	//Se crea la textura del evento interno de la lista de eventos del espacio indicado por el indiceTextura
        	    	//Se deja un tiempo de 800 milisegundos para dar tiempo al Thread del Render a que borre la textura actual
                    Handler handler = new Handler();
        	        handler.postDelayed(new Runnable() {
        	            public void run() {
        	            	//Se crea la textura del evento interno
        	            	crearTexturaEventoInterno(listaEventos.get(indiceTextura), indiceTextura+1, listaEventos.size());
        	            }
        	        }, 800);                    
                	accionActual = "render-evento-interno";
                	texturaCreada = true; 
                	//Se muestra el men� de desplazamiento (Los botones <- ->) para cambiar entre eventos
                	muestraMenuDesplazamiento();
                }       
                
              //Se pregunta si en el men� se hizo clic a la acci�n de ver se�alamientos del espacio de inter�s
                if(this.accionActual.equals("render-senialamiento") && indiceTextura != -10){
                	//Se indica el indiceTextura a -10 para que no vuelva a entrar a crear de nuevo la textura en cada Frame de la c�mara
                	indiceTextura = -10;
                	//Se borra la tetura actual
                	mRenderer.deleteCurrentProductTexture();
                    mRenderer.setRenderState(FeiArRender.RS_LOADING);
                    Handler handler = new Handler();
                	//Se crea la textura del se�alamiento con la lista de indicaciones del espacio de inter�s
        	    	//Se deja un tiempo de 1000 milisegundos para dar tiempo al Thread del Render a que cargue la textura actual                    
        	        handler.postDelayed(new Runnable() {
        	            public void run() {
        	            	crearTexturaIndicacion(listaIndicaciones);
        	            }
        	        }, 1000);                     
                	accionActual = "render-senialamiento";
                	texturaCreada = true;  
                }
            	
                //Si una textura nueva fue creada se avisa al Render que la puede proyectar
            	if(texturaCreada){
            		mRenderer.setRenderState(FeiArRender.RS_NORMAL);
            	}
            	
            	if(!this.accionActual.equals("render-evento-externo")){
            		show2DOverlay();
            	}
            }
		}else{
			hide2DOverlay();
		}
	}
	

    
    //Se agrega el factor de escala para la textura dependendiendo de cada dispositivo
    public void setDeviceDPIScaleFactor(float dpiSIndicator){
        mRenderer.setDPIScaleIndicator(dpiSIndicator);
                
        if(dpiSIndicator <= 1.0f){
        	// ldpi (0.75) y mdpi
        	//mRenderer.setScaleFactor(1.6f);
        	mRenderer.setScaleFactor(0.6f);
        }else if(dpiSIndicator == 1.5f){
        	//hdpi
        	//mRenderer.setScaleFactor(1.3f);
            mRenderer.setScaleFactor(0.4f);        	
        }else if(dpiSIndicator == 2.0f){
        	//xhdpi
        	//mRenderer.setScaleFactor(1.0f);
            mRenderer.setScaleFactor(0.3f);        	
        }else if(dpiSIndicator == 3.0f){
        	//xxhdpi
        	//mRenderer.setScaleFactor(0.6f);
            mRenderer.setScaleFactor(0.2f);
        }else if(dpiSIndicator == 4.0f){
        	//xxxhdpi
        	//mRenderer.setScaleFactor(0.6f);
            mRenderer.setScaleFactor(0.2f);
        }else if(dpiSIndicator == 1.3312501f){
        	//tvdpi
        	mRenderer.setScaleFactor(0.2f);
        }
        
    } 
    
    
    //-----------------------------------------------------------------------------------
    //	Funciones que crean las texturas que son visualizadas en la c�mara del dispositivo
    //-----------------------------------------------------------------------------------
    
    /*
     * Crea la textura del se�alamiento con las indicaciones relacionadas con el espacio de inter�s
     */
    public void crearTexturaIndicacion(ArrayList<Indicacion> indicaciones){
        // Cleans old texture reference if necessary
        if (texturaPrincipal != null){
        	texturaPrincipal = null;
            System.gc();
        }
        
        OverlayViewSenialamiento productView = new OverlayViewSenialamiento(AugmentedReality.this);
        for(Indicacion i : indicaciones){
        	productView.setIndicacion(i.getOrientacion(), i.getDescripcion());
        }
        
        productView.setLayoutParams(new LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT));
        
        // Sets View measure - This size should be the same as the
        // texture generated to display the overlay in order for the
        // texture to be centered in screen
        productView.measure(MeasureSpec.makeMeasureSpec(mTextureSize,
            MeasureSpec.EXACTLY), MeasureSpec.makeMeasureSpec(
            mTextureSize, MeasureSpec.EXACTLY));      
        
        // updates layout size
        productView.layout(0, 0, productView.getMeasuredWidth(),
            productView.getMeasuredHeight());
        
        // Draws the View into a Bitmap. Note we are allocating several
        // large memory buffers thus attempt to clear them as soon as
        // they are no longer required:
        Bitmap bitmap = Bitmap.createBitmap(mTextureSize, mTextureSize, Bitmap.Config.ARGB_8888);
        
        Canvas c = new Canvas(bitmap);
        productView.draw(c);
        
        // Clear the product view as it is no longer needed
        productView = null;
        System.gc();
        
        // Allocate int buffer for pixel conversion and copy pixels
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        
        int[] data = new int[bitmap.getWidth() * bitmap.getHeight()];
        bitmap.getPixels(data, 0, bitmap.getWidth(), 0, 0,
            bitmap.getWidth(), bitmap.getHeight());
        
        // Recycle the bitmap object as it is no longer needed
        bitmap.recycle();
        bitmap = null;
        c = null;
        System.gc();
        
        // Generates the Texture from the int buffer
        texturaPrincipal = Texture.loadTextureFromIntBuffer(data,width, height);
        
        // Clear the int buffer as it is no longer needed
        data = null;
        System.gc();
            
        productTextureIsCreated();        
        
    }
    
    /*
     * Crea la textura de un servicio de un espacio de inter�s
     */
    public void crearTexturaServicio(Servicio servicio, String responsable, int servicioActual, int totalServicios){
        // Cleans old texture reference if necessary
        if (texturaPrincipal != null){
        	texturaPrincipal = null;
            System.gc();
        }
        
        OverlayViewServicio productView = new OverlayViewServicio(AugmentedReality.this);
        productView.setNombre(servicio.getNombre());
        productView.setDescripcion(servicio.getDescripcion());
        productView.setResponsable(responsable);
        productView.setServicioActual(servicioActual);
        productView.setTotalServicios(totalServicios);
     
        productView.setLayoutParams(new LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT));
        
        // Sets View measure - This size should be the same as the
        // texture generated to display the overlay in order for the
        // texture to be centered in screen
        productView.measure(MeasureSpec.makeMeasureSpec(mTextureSize,
            MeasureSpec.EXACTLY), MeasureSpec.makeMeasureSpec(
            mTextureSize, MeasureSpec.EXACTLY));      
        
        // updates layout size
        productView.layout(0, 0, productView.getMeasuredWidth(),
            productView.getMeasuredHeight());
        
        // Draws the View into a Bitmap. Note we are allocating several
        // large memory buffers thus attempt to clear them as soon as
        // they are no longer required:
        Bitmap bitmap = Bitmap.createBitmap(mTextureSize, mTextureSize, Bitmap.Config.ARGB_8888);
        
        Canvas c = new Canvas(bitmap);
        productView.draw(c);
        
        // Clear the product view as it is no longer needed
        productView = null;
        System.gc();
        
        // Allocate int buffer for pixel conversion and copy pixels
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        
        int[] data = new int[bitmap.getWidth() * bitmap.getHeight()];
        bitmap.getPixels(data, 0, bitmap.getWidth(), 0, 0,
            bitmap.getWidth(), bitmap.getHeight());
        
        // Recycle the bitmap object as it is no longer needed
        bitmap.recycle();
        bitmap = null;
        c = null;
        System.gc();
        
        // Generates the Texture from the int buffer
        texturaPrincipal = Texture.loadTextureFromIntBuffer(data,width, height);
        
        // Clear the int buffer as it is no longer needed
        data = null;
        System.gc();
            
        productTextureIsCreated();         
        
    }
    
    /*
     * Crea la textura de un evento externo de un espacio de inter�s
     */
    public void crearTexturaEventoInterno(Evento evento, int eventoActual, int totalEventos){
        // Cleans old texture reference if necessary
        if (texturaPrincipal != null){
        	texturaPrincipal = null;
            System.gc();
        }
        
		// Generates a View to display the data
        OverlayViewEventoInterno productView = new OverlayViewEventoInterno(AugmentedReality.this);
        productView.setNombre(evento.getNombre());
        productView.setDescripcion(evento.getDescripcion());
        productView.setFechaInicio(evento.getFechaInicio());
        productView.setFechaFin(evento.getFechaFin());
        productView.setEventoActual(eventoActual);
        productView.setTotalEventos(totalEventos);
        
        productView.setLayoutParams(new LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT));
        
        // Sets View measure - This size should be the same as the
        // texture generated to display the overlay in order for the
        // texture to be centered in screen
        productView.measure(MeasureSpec.makeMeasureSpec(mTextureSize,
            MeasureSpec.EXACTLY), MeasureSpec.makeMeasureSpec(
            mTextureSize, MeasureSpec.EXACTLY));      
        
        // updates layout size
        productView.layout(0, 0, productView.getMeasuredWidth(),
            productView.getMeasuredHeight());
        
        // Draws the View into a Bitmap. Note we are allocating several
        // large memory buffers thus attempt to clear them as soon as
        // they are no longer required:
        Bitmap bitmap = Bitmap.createBitmap(mTextureSize, mTextureSize, Bitmap.Config.ARGB_8888);
        
        Canvas c = new Canvas(bitmap);
        productView.draw(c);
        
        // Clear the product view as it is no longer needed
        productView = null;
        System.gc();
        
        // Allocate int buffer for pixel conversion and copy pixels
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        
        int[] data = new int[bitmap.getWidth() * bitmap.getHeight()];
        bitmap.getPixels(data, 0, bitmap.getWidth(), 0, 0,
            bitmap.getWidth(), bitmap.getHeight());
        
        // Recycle the bitmap object as it is no longer needed
        bitmap.recycle();
        bitmap = null;
        c = null;
        System.gc();
        
        // Generates the Texture from the int buffer
        texturaPrincipal = Texture.loadTextureFromIntBuffer(data,width, height);
        
        // Clear the int buffer as it is no longer needed
        data = null;
        System.gc();
            
        productTextureIsCreated();          
        
    }
    
    /*
     * Crea la textura de un evento externo
     */    
    public void crearTexturaEventoExterno(Evento evento){
        // Cleans old texture reference if necessary
        if (texturaPrincipal != null){
        	texturaPrincipal = null;
            System.gc();
        }
        
		// Generates a View to display the data
        OverlayViewEventoExterno productView = new OverlayViewEventoExterno(AugmentedReality.this);
        productView.setNombre(evento.getNombre());
        productView.setDescripcion(evento.getDescripcion());
        productView.setLugar(evento.getLugar());
        productView.setFechaInicio(evento.getFechaInicio());
        productView.setFechaFin(evento.getFechaFin());
        
        productView.setLayoutParams(new LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT));
        
        // Sets View measure - This size should be the same as the
        // texture generated to display the overlay in order for the
        // texture to be centered in screen
        productView.measure(MeasureSpec.makeMeasureSpec(mTextureSize,
            MeasureSpec.EXACTLY), MeasureSpec.makeMeasureSpec(
            mTextureSize, MeasureSpec.EXACTLY));      
        
        // updates layout size
        productView.layout(0, 0, productView.getMeasuredWidth(),
            productView.getMeasuredHeight());
        
        // Draws the View into a Bitmap. Note we are allocating several
        // large memory buffers thus attempt to clear them as soon as
        // they are no longer required:
        Bitmap bitmap = Bitmap.createBitmap(mTextureSize, mTextureSize, Bitmap.Config.ARGB_8888);
        
        Canvas c = new Canvas(bitmap);
        productView.draw(c);
        
        // Clear the product view as it is no longer needed
        productView = null;
        System.gc();
        
        // Allocate int buffer for pixel conversion and copy pixels
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        
        int[] data = new int[bitmap.getWidth() * bitmap.getHeight()];
        bitmap.getPixels(data, 0, bitmap.getWidth(), 0, 0,
            bitmap.getWidth(), bitmap.getHeight());
        
        // Recycle the bitmap object as it is no longer needed
        bitmap.recycle();
        bitmap = null;
        c = null;
        System.gc();
        
        // Generates the Texture from the int buffer
        texturaPrincipal = Texture.loadTextureFromIntBuffer(data,width, height);
        
        // Clear the int buffer as it is no longer needed
        data = null;
        System.gc();
            
        productTextureIsCreated();        
        
    }
    
    /*
     * Crea la textura de un espacio de inter�s
     */    
    public void crearTexturaEspacioDeInteres(EspacioDeInteres espacio){
        
        // Cleans old texture reference if necessary
        if (texturaPrincipal != null){
        	texturaPrincipal = null;
            System.gc();
        }
        
		// Generates a View to display the data
        OverlayViewEspacioDeInteres productView = new OverlayViewEspacioDeInteres(AugmentedReality.this);
        productView.setNombre(espacio.getNombre());
        productView.setDescripcion(espacio.getDescripcion());
        
        productView.setLayoutParams(new LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,RelativeLayout.LayoutParams.WRAP_CONTENT));
        
        // Sets View measure - This size should be the same as the
        // texture generated to display the overlay in order for the
        // texture to be centered in screen
        productView.measure(MeasureSpec.makeMeasureSpec(mTextureSize,
            MeasureSpec.EXACTLY), MeasureSpec.makeMeasureSpec(
            mTextureSize, MeasureSpec.EXACTLY));      
        
        // updates layout size
        productView.layout(0, 0, productView.getMeasuredWidth(),
            productView.getMeasuredHeight());
        
        // Draws the View into a Bitmap. Note we are allocating several
        // large memory buffers thus attempt to clear them as soon as
        // they are no longer required:
        Bitmap bitmap = Bitmap.createBitmap(mTextureSize, mTextureSize, Bitmap.Config.ARGB_8888);
        
        Canvas c = new Canvas(bitmap);
        productView.draw(c);
        
        // Clear the product view as it is no longer needed
        productView = null;
        System.gc();
        
        // Allocate int buffer for pixel conversion and copy pixels
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        
        int[] data = new int[bitmap.getWidth() * bitmap.getHeight()];
        bitmap.getPixels(data, 0, bitmap.getWidth(), 0, 0,
            bitmap.getWidth(), bitmap.getHeight());
        
        // Recycle the bitmap object as it is no longer needed
        bitmap.recycle();
        bitmap = null;
        c = null;
        System.gc();
        
        // Generates the Texture from the int buffer
        texturaPrincipal = Texture.loadTextureFromIntBuffer(data,width, height);
        
        // Clear the int buffer as it is no longer needed
        data = null;
        System.gc();
            
        productTextureIsCreated();
        
    } 
    
    
    //----------------------------------------------------------------------
    //   Funciones que buscan los datos relacionados con el marcador analizado
    // --------------------------------------------------------------------
    
    /*
     * Busca el espacio de inter�s relacionado con un marcador
     */
    public EspacioDeInteres buscarEspacioDeInteres(int idMarcadorEncontrado){
    	EspacioDeInteresController controller = new EspacioDeInteresController(AugmentedReality.this);
    	EspacioDeInteres espacio = controller.getEspacioDeInteresPorIdMarcador(idMarcadorEncontrado);
    	controller.cerrarConexion();
    	return espacio;
    	
    }        
    
    /*
     * Busca los servicios de un espacio de inter�s
     */
    public void buscarServiciosDeEspacioDeInteres(int idEspacio){
    	ServicioController controller = new ServicioController(AugmentedReality.this);
    	listaServicios.clear();
    	listaServicios = controller.getServiciosDeEspacioDeInteres(idEspacio);
    	controller.cerrarConexion();
    	ResponsableController respController = new ResponsableController(AugmentedReality.this);
    	listaNombresResponsables.clear();
    	String nombre = "";
    	for(Servicio servicio : listaServicios){
    		nombre =  respController.getNombreDeResponsable(servicio.getIdResponsable());
    		listaNombresResponsables.add(nombre);
    	}
    	respController.cerrarConexion();
    	indiceTextura = 0;
    }
    
    /*
     * Busca los eventos internos de un espacio de inter�s
     */
    public void buscarEventosInternosDeEspacioDeInteres(int idEspacio){
    	EventoController controller = new EventoController(AugmentedReality.this);
    	listaEventos.clear();
    	listaEventos = controller.getEventosInternosDeEspacioDeInteres(idEspacio);
    	controller.cerrarConexion();
    	this.indiceTextura = 0;
    }
    
    /*
     * Busca las indicaciones de un espacio de inter�s
     */
    public void buscarIndicacionesDeEspacioDeInteres(int idEspacio){
    	IndicacionController controller = new IndicacionController(AugmentedReality.this);
    	listaIndicaciones.clear();
    	listaIndicaciones = controller.getIndicacionesDeEspacioDeInteres(idEspacio);
    	controller.cerrarConexion();
    	this.indiceTextura = 0;
    }

    /*
     * Busca un evento externo relacionado con un marcador
     */
    public Evento buscarEventoExterno(int idMarcadorEncontrado){
    	EventoController controller = new EventoController(AugmentedReality.this);
    	Evento evento = controller.getEventoExternoPorIdMarcador(idMarcadorEncontrado);
    	controller.cerrarConexion();
    	return evento;
    	
    }       
    
    /**
     * Function to generate the OpenGL Texture Object in the renderFrame thread
     */
    public void productTextureIsCreated()
    {
    	mRenderer.setRenderState(FeiArRender.RS_TEXTURE_GENERATED);
    }    
    
    /** Returns the current Texture */
    public Texture getProductTexture()
    {
        return texturaPrincipal;
    }     
    
    
    //----------------------------------------------------------------------------
    //  Funciones relacionadas con la interf�z que se despliega sobre la c�mara
    //----------------------------------------------------------------------------
    
    
    private void startLoadingAnimation()
    {
    	//Despliega la interfaz sobre la pantalla de la c�mara
    	LayoutInflater inflater = LayoutInflater.from(this);
    	this.mUILayout = (RelativeLayout) inflater.inflate(R.layout.camera_overlay, null); 
        mUILayout.setVisibility(View.VISIBLE);
        mUILayout.setBackgroundColor(Color.BLACK);
        
        //Se muestra el dialogo de carga
        loadingDialogHandler.mLoadingDialogContainer = mUILayout.findViewById(R.id.loading_layout);
        loadingDialogHandler.mLoadingDialogContainer.setVisibility(View.VISIBLE);
        
        addContentView(mUILayout, new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT));

        mStatusBar = (TextView) mUILayout.findViewById(R.id.overlay_status);        

        menu = (LinearLayout) mUILayout.findViewById(R.id.linlayMenu);
        menuDesplazamiento = (LinearLayout) mUILayout.findViewById(R.id.linlayDesplazamiento);
        
        loadingDialogHandler.sendEmptyMessage(SHOW_LOADING_DIALOG);  
        
        //El evento clic del boton "descripci�n" de la interfaz
        ivDescripcion = (ImageView) mUILayout.findViewById(R.id.ivDescripcion);
        ivDescripcion.setOnClickListener(new OnClickListener(){
        	public void onClick(View v){
        		escondeMenuDesplazamiento();
        		accionActual = "render-espacio";
        		ultimoIdMarcadorEncontrado = -1;
        	}
        });
        
      //El evento clic del boton "servicios" de la interfaz
        ivServicios = (ImageView) mUILayout.findViewById(R.id.ivServicios);
        ivServicios.setOnClickListener(new OnClickListener(){
        	public void onClick(View v){
    			if(listaServicios.size() == 0){
        			mStatusBar.setText("No hay servicios");
        			showStatusBar();
				}else{
	        		accionActual = "render-servicio";
	        		indiceTextura = 0;
	        		ultimoIndiceTextura = -1;
				}        		
        	}
        });
        
      //El evento clic del boton "eventos internos" de la interfaz
        ivEventos = (ImageView) mUILayout.findViewById(R.id.ivEventos);
        ivEventos.setOnClickListener(new OnClickListener(){
        	public void onClick(View v){
    			if(listaEventos.size() == 0){
        			mStatusBar.setText("No hay eventos");
        			showStatusBar();
				}else{
	        		accionActual = "render-evento-interno";
	        		indiceTextura = 0;
	        		ultimoIndiceTextura = -1;
				}
        	}
        });
        
      //El evento clic del boton "se�alamientos" de la interfaz
        ivSenialamiento = (ImageView) mUILayout.findViewById(R.id.ivSenialamiento);
        ivSenialamiento.setOnClickListener(new OnClickListener(){
        	public void onClick(View v){
    			if(listaIndicaciones.size() == 0){
        			mStatusBar.setText("No hay se�alamiento");
        			showStatusBar();
				}else{
					escondeMenuDesplazamiento();
	        		accionActual = "render-senialamiento";
				}        		
        		
        	}
        });
        
      //El evento clic del boton "sitio web relacionado" de la interfaz
        ivSitioWeb = (ImageView) mUILayout.findViewById(R.id.ivSitioWeb);
        ivSitioWeb.setOnClickListener(new OnClickListener(){
        	public void onClick(View v){
        		//urlEventoInterno intent para abrir pagina en el navegador
        		if(!urlEventoInterno.equals("") && accionActual.equals("render-evento-interno")){
        		      Intent viewIntent = new Intent("android.intent.action.VIEW", Uri.parse(urlEventoInterno));
        		      startActivity(viewIntent);
        		}else{
            		if(!urlEspacio.equals("") && accionActual.equals("render-espacio")){
            		      Intent viewIntent = new Intent("android.intent.action.VIEW", Uri.parse(urlEspacio));
            		      startActivity(viewIntent);
      	      		}else{
      	        		if(!urlEventoExterno.equals("") && accionActual.equals("render-evento-externo")){
      	      		      Intent viewIntent = new Intent("android.intent.action.VIEW", Uri.parse(urlEventoExterno));
      	      		      startActivity(viewIntent);
      	        		}else{
      	        			mStatusBar.setText("No hay un sitio web relacionado");
      	        			showStatusBar();
      	        		}            	      			
      	      		}        			
        		}  		
        	}
        });
        
        //El evento clic del boton "Siguiente" de la interfaz (Cambia al siguiente servicio u evento interno)
        ivSiguiente = (ImageView) mUILayout.findViewById(R.id.ivSiguiente);
        ivSiguiente.setOnClickListener(new OnClickListener(){
        	public void onClick(View v){
        		if(accionActual.equals("render-servicio")){
        			if(listaServicios.size() == 0){
  	        			mStatusBar.setText("No hay servicios");
  	        			showStatusBar();
        			}else{
            			if(listaServicios.size() == indiceTextura+1){
            				indiceTextura=0;
            			}else{
            				indiceTextura++;
            			}        				
        			}
        		}else{
        			if(accionActual.equals("render-evento-interno")){
        				if(listaEventos.size() == 0){
      	        			mStatusBar.setText("No hay eventos");
      	        			showStatusBar();
        				}else{
                			if(listaEventos.size() == indiceTextura+1){
                				indiceTextura=0;
                			}else{
                				indiceTextura++;
                			}  	
        				}      				
        			}
        		}
        	}
        });

      //El evento clic del boton "Anterior" de la interfaz (Cambia al anterior servicio u evento interno)
        ivAnterior = (ImageView) mUILayout.findViewById(R.id.ivAnterior);
        ivAnterior.setOnClickListener(new OnClickListener(){
        	public void onClick(View v){
        		if(accionActual.equals("render-servicio")){
        			if(listaServicios.size() == 0){
  	        			mStatusBar.setText("No hay servicios");
  	        			showStatusBar();
        			}else{
            			if(-1 == indiceTextura-1){
            				indiceTextura = listaServicios.size() - 1;
            			}else{
            				indiceTextura--;
            			}	
        			}
        		}else{
        			if(accionActual.equals("render-evento-interno")){
        				if(listaEventos.size() == 0){
      	        			mStatusBar.setText("No hay eventos");
      	        			showStatusBar();
        				}else{
                			if(-1 == indiceTextura-1){
                				indiceTextura = listaEventos.size() - 1;
                			}else{
                				indiceTextura--;
                			}    
        				}        				    				
        			}
        		}
        	}
        });              
        
        //Esconde el men� sobre la c�mara que solo se despliega cuando encuentra un marcador con informaci�n asociada
        hide2DOverlay();
        hideStatusBar();
    }    
    
    
    /*
     * Despliega el men� encima de la c�mara
     */
    public void show2DOverlay(){
        menu.setVisibility(View.VISIBLE);
    }
    
    /*
     * Despliega los botones de desplazamiento por encima de la c�mara
     */
    public void muestraMenuDesplazamiento(){
    	menuDesplazamiento.setVisibility(View.VISIBLE);
    }

    /*
     * Esconde el men� encima de la c�mara
     */    
    public void escondeMenuDesplazamiento(){
    	menuDesplazamiento.setVisibility(View.GONE);
    }
    
    /*
     * Esconde los botones de desplazamiento y el men� de acciones
     */
    public void hide2DOverlay(){
    	menuDesplazamiento.setVisibility(View.GONE);
        menu.setVisibility(View.GONE);
    }    
    
    
    /*
     * Esconde el cuadro de texto de estado de la c�mara
     */
    public void hideStatusBar(){
        if (mStatusBar.getVisibility() == View.VISIBLE){
        	mStatusBar.setVisibility(View.GONE);
        }
    }
    
    
    /*
     * Muestra el cuadro de texto de estado en la c�mara
     */
    public void showStatusBar(){	
        if (mStatusBar.getVisibility() == View.GONE){
        	mStatusBar.setVisibility(View.VISIBLE);
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                	mStatusBar.setVisibility(View.GONE);
                }
            }, 2000);        	
        }
    } 
     
    
    
    static class LoadingDialogHandler extends Handler
    {
    	private final WeakReference<AugmentedReality> wrAR;

        
        public View mLoadingDialogContainer;
        
        
        LoadingDialogHandler(AugmentedReality ar)
        {
        	wrAR = new WeakReference<AugmentedReality>(ar);
        }
        
        
        public void handleMessage(Message msg)
        {
        	AugmentedReality ar = wrAR.get();
            if (ar == null)
            {
                return;
            }
            
            if (msg.what == SHOW_LOADING_DIALOG)
            {
                mLoadingDialogContainer.setVisibility(View.VISIBLE);
                
            } else if (msg.what == HIDE_LOADING_DIALOG)
            {
                mLoadingDialogContainer.setVisibility(View.GONE);
            }
        }    	
    }    
    
}
