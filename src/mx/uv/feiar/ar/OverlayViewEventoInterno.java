package mx.uv.feiar.ar;

import mx.uv.feiar.R;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class OverlayViewEventoInterno extends RelativeLayout{

    public OverlayViewEventoInterno(Context context){
        this(context, null);
    }
    
    public OverlayViewEventoInterno(Context context, AttributeSet attrs){
        this(context, attrs, 0);
    }    
    
    public OverlayViewEventoInterno(Context context, AttributeSet attrs, int defStyle){
        super(context, attrs, defStyle);
        inflateLayout(context);
    }    
    
    private void inflateLayout(Context context){
        final LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.bitmap_layout_evento_interno, this, true);
    }   
    
    public void setNombre(String nombre){
        TextView tv = (TextView) findViewById(R.id.tvNombreEventoInterno);
        tv.setText(nombre);
    }    
    
    public void setDescripcion(String descripcion){
        TextView tv = (TextView) findViewById(R.id.tvDescripcionEventoInterno);
        tv.setText(descripcion);
    }
         

    public void setFechaInicio(String fechaInicio){
		String fechaIni = fechaInicio.split(" ")[0];
		String horaInicio = fechaInicio.split(" ")[1];
		horaInicio = horaInicio.substring(0, 5);	    	
    	
        TextView tv = (TextView) findViewById(R.id.tvEventoInternoFechaInicio);
        tv.setText("El "+ fechaIni + " a las " + horaInicio);
    }        
    
    public void setFechaFin(String fechaFin){
		String fechaF = fechaFin.split(" ")[0];
		String horaFin = fechaFin.split(" ")[1];
		horaFin = horaFin.substring(0, 5);    	
    	
        TextView tv = (TextView) findViewById(R.id.tvEventoInternoFechaFin);
        tv.setText("El "+ fechaF + " a las " + horaFin);
    }             
    
    public void setEventoActual(int actual){
        TextView tv = (TextView) findViewById(R.id.tvEventoInternoActual);
        tv.setText(""+actual);    	
    }

    public void setTotalEventos(int total){
        TextView tv = (TextView) findViewById(R.id.tvTotalEventosInternos);
        tv.setText(""+total);    	
    }    
    
	
}
