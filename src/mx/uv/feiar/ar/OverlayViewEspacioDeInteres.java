package mx.uv.feiar.ar;

import mx.uv.feiar.R;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class OverlayViewEspacioDeInteres extends RelativeLayout{
	
    public OverlayViewEspacioDeInteres(Context context){
        this(context, null);
    }
    
    public OverlayViewEspacioDeInteres(Context context, AttributeSet attrs){
        this(context, attrs, 0);
    }    
    
    public OverlayViewEspacioDeInteres(Context context, AttributeSet attrs, int defStyle){
        super(context, attrs, defStyle);
        inflateLayout(context);
    }    
    
    private void inflateLayout(Context context){
        final LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.bitmap_layout_espacio_de_interes, this, true);
    }    
    
    public void setNombre(String nombre){
        TextView tv = (TextView) findViewById(R.id.tvEspacioDeInteresNombre);
        tv.setText(nombre);
    }    
    
    public void setDescripcion(String descripcion){
        TextView tv = (TextView) findViewById(R.id.tvEspacioDeInteresDescripcion);
        tv.setText(descripcion);
    }    
    
}
