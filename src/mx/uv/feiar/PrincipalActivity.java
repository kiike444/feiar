package mx.uv.feiar;

import java.util.ArrayList;

import mx.uv.feiar.ar.AugmentedReality;
import mx.uv.feiar.controller.DependenciaController;
import mx.uv.feiar.controller.EspacioDeInteresController;
import mx.uv.feiar.controller.EventoController;
import mx.uv.feiar.model.Evento;
import android.support.v7.app.ActionBarActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

public class PrincipalActivity extends ActionBarActivity {
	private EventoController controller;
	private ListView listvEventos;
	private TextView txtEmpty;
	private ArrayList<Object> objetos;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_principal);
		
		txtEmpty = (TextView) findViewById(R.id.txtEmpty);
		CargarListaEventos proceso = new CargarListaEventos(this);
		proceso.execute();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.principal, menu);
		return true;
	}
	
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		  if (v.getId()==R.id.listvEventos) {
		    AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
		    menu.setHeaderTitle("Opciones");
		    String[] menuItems = new String[1];
		    menuItems[0] = "Compartir"; 
		    for (int i = 0; i<menuItems.length; i++) {
		      menu.add(Menu.NONE, i, i, menuItems[i]);
		    }
		  }
		}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
	  AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
	  Object o = objetos.get(info.position);
	  int menuItemIndex = item.getItemId();
	  String[] menuItems = getResources().getStringArray(R.array.MenuContextualPrincipal);
	  String menuItemName = menuItems[menuItemIndex];
	  switch (menuItemName) {
		case "Compartir":
			if(o instanceof Evento){
				Evento evento = (Evento)o;
				String fechaInicio = evento.getFechaInicio().split(" ")[0];
				String horaInicio = evento.getFechaInicio().split(" ")[1];
				horaInicio = horaInicio.substring(0, 5);		

				String fechaFin = evento.getFechaInicio().split(" ")[0];
				String horaFin = evento.getFechaInicio().split(" ")[1];
				horaFin = horaInicio.substring(0, 5);					
				
				String contenido = evento.getNombre();
				contenido += "\n" + evento.getDescripcion();
				if(evento.getLugar() != null){
					contenido += "\nSe celebrará en " + evento.getLugar() + ".";
				}else{
					EspacioDeInteresController controller = new EspacioDeInteresController(PrincipalActivity.this);
					String lugar = controller.getNombreEspacioDeInteresPorId(evento.getIdEspacioDeInteres());
					controller.cerrarConexion();
					contenido += "\nSe celebrará en " + lugar + ".";
				}
				
				contenido += "\nInicia el " + fechaInicio + " a las "+ horaInicio + " horas.";
				contenido += "\nFinaliza el " + fechaFin + " a las "+ horaFin + " horas.";
				Intent intentCompartir = new Intent(Intent.ACTION_SEND);
				intentCompartir.setType("text/plain");
				intentCompartir.putExtra(Intent.EXTRA_SUBJECT, evento.getNombre());
				intentCompartir.putExtra(Intent.EXTRA_TEXT, contenido);
				startActivity(Intent.createChooser(intentCompartir , "Compartir"));
			}else{
				
			}
			break;
		}
	  
	  return true;
	}	

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_ar) {
			startActivity(new Intent(this, AugmentedReality.class));
		}
		if (id == R.id.action_update) {
			startActivity(new Intent(this, ActualizarActivity.class));
		}		
		return super.onOptionsItemSelected(item);
	}
	
	
	
	private class CargarListaEventos extends AsyncTask<Void, Void, Void>{
		private ProgressDialog dialog;
		private Context context;
		private String nombre = "";
		
	    public CargarListaEventos(Context context) {
	        dialog = new ProgressDialog(context);
	        this.context = context;
	    }		
		
	    @Override
	    protected void onPreExecute() {
	        dialog.setMessage("Cargando lista de eventos");
	        dialog.show();
	    }		
		
		@Override
		protected Void doInBackground(Void... params) {
			DependenciaController dependenciaController = new DependenciaController(context);
			nombre = dependenciaController.getNombreDependenciaAsoaciada();

			
			listvEventos = (ListView)findViewById(R.id.listvEventos);
			controller = new EventoController(context);
			ArrayList<Evento> eventos = controller.getTodosLosEventos();
			objetos = new ArrayList<Object>();
			for(Evento e : eventos){
				objetos.add(e);
			}
			
			return null;
		}
		
	    @Override
	    protected void onPostExecute(Void result) {
	        if (dialog.isShowing()) {
	            dialog.dismiss();
	        }
			EventosAdapter adaptador = new EventosAdapter(context, objetos);
			listvEventos.setAdapter(adaptador);

			
			listvEventos.setEmptyView(txtEmpty);
	        registerForContextMenu(listvEventos);
			if(!nombre.equals("")){
				setTitle(nombre);	
				txtEmpty.setText("No hay eventos ni avisos registrados");
			}else{
				txtEmpty.setText("Bienvenido a FEI AR"+ System.getProperty ("line.separator") +"Debes descargar los datos de tu dependencia para comenzar");
			}
	    }		
		
	}
}
