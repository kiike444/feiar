package mx.uv.feiar.descargadatos;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import mx.uv.feiar.controller.ActualizacionController;
import mx.uv.feiar.controller.AvisoController;
import mx.uv.feiar.controller.DependenciaController;
import mx.uv.feiar.controller.EspacioDeInteresController;
import mx.uv.feiar.controller.EventoController;
import mx.uv.feiar.controller.IndicacionController;
import mx.uv.feiar.controller.ResponsableController;
import mx.uv.feiar.controller.ServicioController;
import mx.uv.feiar.model.Actualizacion;
import mx.uv.feiar.model.Aviso;
import mx.uv.feiar.model.Dependencia;
import mx.uv.feiar.model.EspacioDeInteres;
import mx.uv.feiar.model.Evento;
import mx.uv.feiar.model.Indicacion;
import mx.uv.feiar.model.Responsable;
import mx.uv.feiar.model.Servicio;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class DescargaDeDatos {

	private Context context;
	private String nombreDependencia ="";
	//private String BASE_PATH = "http://192.168.1.66/feiar/index.php/ServiciosWeb/";	
	//private String BASE_PATH = "http://172.72.40.184/feiar/index.php/ServiciosWeb/";	
	private String BASE_PATH = "http://feiar.esy.es/feiar/index.php/ServiciosWeb/";	
	//http://feiar.esy.es/feiar/
	private String BASE_PATH_SERVIDOR_AVISOS = "";
	private DependenciaController dependenciaController;
	private EspacioDeInteresController espacioController;
	private ServicioController servicioController;
	private ResponsableController responsableController;
	private EventoController eventoController;
	private IndicacionController indicacionController;
	private ActualizacionController actualizacionController;	
	private AvisoController avisoController;
	
	
	public DescargaDeDatos(Context context){
		this.context = context;
	}
	
	public DescargaDeDatos(Context context, String basePathServidorAvisos){
		this.context = context;
		this.BASE_PATH_SERVIDOR_AVISOS = basePathServidorAvisos;
	}
	
	public String getNombreDependencia(){
		return nombreDependencia;
	}
	
	/***
	 * Funci�n que consulta mediate un servicio web si se est� o no en las coordenadas
	 *  una dependencia registrada en el sistema FEI AR
	 * @param latitud
	 * @param longitud
	 * @return ID de la dependencia, -1 si no se encuentra una dependencia en las coordenadas 
	 * o -2 si no es posible conectarse al servidor
	 */
	public int comprobarDependenciaEnUbicacion(double latitud, double longitud){
		int idDependencia = -1;
		Dependencia dependencia = null;
		HttpClient cliente = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost(BASE_PATH + "estoyEnDependencia/format/json");
        List<NameValuePair> parametros = new ArrayList<NameValuePair>();
        parametros.add(new BasicNameValuePair("latitud", Double.toString(latitud) ));
        parametros.add(new BasicNameValuePair("longitud", Double.toString(longitud) ));		
        try{
        	httpPost.setEntity(new UrlEncodedFormEntity(parametros));
        	HttpResponse resp = cliente.execute(httpPost);
        	HttpEntity ent = resp.getEntity();
        	String respuesta = EntityUtils.toString(ent);
        	if(respuesta.length() > 4){
            	Gson gson = new Gson();
            	dependencia = gson.fromJson(respuesta, Dependencia.class);
            	idDependencia = dependencia.getIdDependencia();
            	this.nombreDependencia = dependencia.getNombre();
        	}else{
            	respuesta = respuesta.replace('"', ' ').trim();
            	idDependencia = Integer.parseInt(respuesta);
        	}        	
        }catch(Exception e){
        	e.printStackTrace();
        	idDependencia = -2;
        }	      
    	return idDependencia;
	}	
	
	/**
	 * Funci�n que descarga y almacena los datos de la dependencia
	 * @param idDependencia
	 * @return TRUE si se realiz� correctamente, FALSE de lo contrario
	 */
	public boolean actualizarDatosDependencia(int idDependencia){
		dependenciaController = new DependenciaController(context);
		dependenciaController.eliminarDatosDependencia();
		Dependencia dependencia = null;
		boolean status = false;
		HttpClient cliente = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost(BASE_PATH + "getDependencia/format/json");
        List<NameValuePair> parametros = new ArrayList<NameValuePair>();
        parametros.add(new BasicNameValuePair("idDependencia", Double.toString(idDependencia) ));		
        try{
        	httpPost.setEntity(new UrlEncodedFormEntity(parametros, HTTP.UTF_8));
        	HttpResponse resp = cliente.execute(httpPost);
        	HttpEntity ent = resp.getEntity();
        	String respuesta = EntityUtils.toString(ent, HTTP.UTF_8);
        	if(respuesta.length() > 0){
            	Gson gson = new Gson();
            	dependencia = gson.fromJson(respuesta, Dependencia.class);
            	status = dependenciaController.guardar(dependencia);        		
        	}
        }catch(Exception e){
        	e.printStackTrace();
        }	              
		return status;
	}	
	
	/**
	 * Funci�n que descarga y almacena los espacios de inter�s de una dependencia
	 * @param idDependencia
	 * @return TRUE si se realiz� correctamente, FALSE de lo contrario
	 */
	public boolean actualizarDatosEspaciosDeInteres(int idDependencia){
		espacioController = new EspacioDeInteresController(context);
		espacioController.eliminarDatosEspacioDeInteres();
		List<EspacioDeInteres> espacios = null;
		boolean status = false;
		HttpClient cliente = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost(BASE_PATH + "getEspaciosDeInteres/format/json");
        List<NameValuePair> parametros = new ArrayList<NameValuePair>();
        parametros.add(new BasicNameValuePair("idDependencia", Double.toString(idDependencia) ));			
        try{
        	httpPost.setEntity(new UrlEncodedFormEntity(parametros, HTTP.UTF_8));
        	HttpResponse resp = cliente.execute(httpPost);
        	HttpEntity ent = resp.getEntity();
        	String respuesta = EntityUtils.toString(ent, HTTP.UTF_8);
        	if(respuesta.length() > 0){
            	Gson gson = new Gson();
            	Type listType = new TypeToken<List<EspacioDeInteres>>(){}.getType();
            	espacios = gson.fromJson(respuesta, listType);
            	for(EspacioDeInteres espacio : espacios){
            		status = espacioController.guardar(espacio);
            	}        		
        	}
        }catch(Exception e){
        	e.printStackTrace();
        }	              
		return status;        
	}	
	
	/**
	 * Funci�n que descarga y almacena los servicios de una dependencia
	 * @param idDependencia
	 * @return TRUE si se realiz� correctamente, FALSE de lo contrario
	 */
	public boolean actualizarDatosServicios(int idDependencia){
		servicioController = new ServicioController(context);
		servicioController.eliminarDatosServicio();
		List<Servicio> servicios = null;
		boolean status = false;
		HttpClient cliente = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost(BASE_PATH + "getServicios/format/json");
        List<NameValuePair> parametros = new ArrayList<NameValuePair>();
        parametros.add(new BasicNameValuePair("idDependencia", Double.toString(idDependencia) ));	
        try{
        	httpPost.setEntity(new UrlEncodedFormEntity(parametros, HTTP.UTF_8));
        	HttpResponse resp = cliente.execute(httpPost);
        	HttpEntity ent = resp.getEntity();
        	String respuesta = EntityUtils.toString(ent, HTTP.UTF_8);
        	if(respuesta.length() > 0){
            	Gson gson = new Gson();
            	Type listType = new TypeToken<List<Servicio>>(){}.getType();
            	servicios = gson.fromJson(respuesta, listType);
            	for(Servicio servicio : servicios){
            		status = servicioController.guardar(servicio);
            	}        		
        	}
        }catch(Exception e){
        	e.printStackTrace();
        }	              
		return status;           
	}	
	
	/**
	 * Funci�n que descarga y alamacena los responsables de servicios de una dependencia
	 * @param idDependencia
	 * @return TRUE si se realiz� correctamente, FALSE de lo contrario
	 */
	public boolean actualizarDatosResponsables(int idDependencia){
		responsableController = new ResponsableController(context);
		responsableController.eliminarDatosResponsable();
		List<Responsable> responsables = null;
		boolean status = false;
		HttpClient cliente = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost(BASE_PATH + "getResponsables/format/json");
        List<NameValuePair> parametros = new ArrayList<NameValuePair>();
        parametros.add(new BasicNameValuePair("idDependencia", Double.toString(idDependencia) ));	
        try{
        	httpPost.setEntity(new UrlEncodedFormEntity(parametros, HTTP.UTF_8));
        	HttpResponse resp = cliente.execute(httpPost);
        	HttpEntity ent = resp.getEntity();
        	String respuesta = EntityUtils.toString(ent, HTTP.UTF_8);
        	if(respuesta.length() > 0){
            	Gson gson = new Gson();
            	Type listType = new TypeToken<List<Responsable>>(){}.getType();
            	responsables = gson.fromJson(respuesta, listType);
            	for(Responsable responsable : responsables){
            		status = responsableController.guardar(responsable);
            	}        		
        	}
        }catch(Exception e){
        	e.printStackTrace();
        }	              
		return status; 		
	}	
	
	/**
	 * Funci�n que descarga y almacena los eventos internos de una dependencia
	 * @param idDependencia
	 * @return TRUE si se realiz� correctamente, FALSE de lo contrario
	 */
	public boolean actualizarDatosEventosInternos(int idDependencia){
		eventoController = new EventoController(context);
		eventoController.eliminarDatosEventoInterno();
		List<Evento> eventos = null;
		boolean status = false;
		HttpClient cliente = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost(BASE_PATH + "getEventosInternos/format/json");
        List<NameValuePair> parametros = new ArrayList<NameValuePair>();
        parametros.add(new BasicNameValuePair("idDependencia", Double.toString(idDependencia) ));	
        try{
        	httpPost.setEntity(new UrlEncodedFormEntity(parametros, HTTP.UTF_8));
        	HttpResponse resp = cliente.execute(httpPost);
        	HttpEntity ent = resp.getEntity();
        	String respuesta = EntityUtils.toString(ent, HTTP.UTF_8);
        	if(respuesta.length() > 0){
            	Gson gson = new Gson();
            	Type listType = new TypeToken<List<Evento>>(){}.getType();
            	eventos = gson.fromJson(respuesta, listType);
            	for(Evento evento : eventos){
            		evento.setIdMarcador(-1);
            		status = eventoController.guardar(evento);
            	}        		
        	}
        }catch(Exception e){
        	e.printStackTrace();
        }	              
		return status;           
	}		
	
	/**
	 * Funci�n que descarga y almacena los eventos externos de una dependencia
	 * @param idDependencia
	 * @return TRUE si se realiz� correctamente, FALSE de lo contrario
	 */
	public boolean actualizarDatosEventosExternos(int idDependencia){
		eventoController = new EventoController(context);
		eventoController.eliminarDatosEventoExterno();
		List<Evento> eventos = null;
		boolean status = false;
		HttpClient cliente = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost(BASE_PATH + "getEventosExternos/format/json");
        List<NameValuePair> parametros = new ArrayList<NameValuePair>();
        parametros.add(new BasicNameValuePair("idDependencia", Double.toString(idDependencia) ));	
        try{
        	httpPost.setEntity(new UrlEncodedFormEntity(parametros, HTTP.UTF_8));
        	HttpResponse resp = cliente.execute(httpPost);
        	HttpEntity ent = resp.getEntity();
        	String respuesta = EntityUtils.toString(ent, HTTP.UTF_8);
        	if(respuesta.length() > 0){
            	Gson gson = new Gson();
            	Type listType = new TypeToken<List<Evento>>(){}.getType();
            	eventos = gson.fromJson(respuesta, listType);
            	for(Evento evento : eventos){
            		status = eventoController.guardar(evento);
            	}        		
        	}
        }catch(Exception e){
        	e.printStackTrace();
        }
		return status;           
	}		
	
	/**
	 * Funci�n que descarga y almacena las indicaciones de los se�alamientos de una dependencia
	 * @param idDependencia
	 * @return TRUE si se realiz� correctamente, FALSE de lo contrario
	 */
	public boolean actualizarDatosIndicaciones(int idDependencia){
		indicacionController = new IndicacionController(context);
		indicacionController.eliminarDatosIndicacion();
		List<Indicacion> indicaciones = null;
		boolean status = false;
		HttpClient cliente = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost(BASE_PATH + "getIndicaciones/format/json");
        List<NameValuePair> parametros = new ArrayList<NameValuePair>();
        parametros.add(new BasicNameValuePair("idDependencia", Double.toString(idDependencia) ));	
        try{
        	httpPost.setEntity(new UrlEncodedFormEntity(parametros, HTTP.UTF_8));
        	HttpResponse resp = cliente.execute(httpPost);
        	HttpEntity ent = resp.getEntity();
        	String respuesta = EntityUtils.toString(ent, HTTP.UTF_8);
        	if(respuesta.length() > 0){
            	Gson gson = new Gson();
            	Type listType = new TypeToken<List<Indicacion>>(){}.getType();
            	indicaciones = gson.fromJson(respuesta, listType);
            	for(Indicacion indicacion : indicaciones){
            		status = indicacionController.guardar(indicacion);
            	}        		
        	}
        }catch(Exception e){
        	e.printStackTrace();
        }	              
		return status;           
	}		
	
	/**
	 * Funci�n que descarga y almacena la tabla de control de actualizaciones de una dependencia
	 * @param idDependencia
	 * @return TRUE si se realiz� correctamente, FALSE de lo contrario
	 */
	public boolean actualizarControlActualizaciones(int idDependencia){
		actualizacionController = new ActualizacionController(context);
		actualizacionController.eliminarDatosActualizacion();
		Actualizacion actualizacion = null;
		boolean status = false;
		HttpClient cliente = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost(BASE_PATH + "getActualizaciones/format/json");
        List<NameValuePair> parametros = new ArrayList<NameValuePair>();
        parametros.add(new BasicNameValuePair("idDependencia", Double.toString(idDependencia) ));	
        try{
        	httpPost.setEntity(new UrlEncodedFormEntity(parametros, HTTP.UTF_8));
        	HttpResponse resp = cliente.execute(httpPost);
        	HttpEntity ent = resp.getEntity();
        	String respuesta = EntityUtils.toString(ent, HTTP.UTF_8);
        	if(respuesta.length() > 0){
            	Gson gson = new Gson();
            	actualizacion = gson.fromJson(respuesta, Actualizacion.class);
            	status = actualizacionController.guardar(actualizacion);        		
        	}
        }catch(Exception e){
        	e.printStackTrace();
        }	              
		return status;           
	}	
	
	/**
	 * Funci�n que obtine la tabla de control de actualizaciones de una dependencia
	 * @param idDependencia
	 * @return Actualizacion
	 */
	public Actualizacion obtenerControlActualizaciones(int idDependencia){
		Actualizacion actualizacion = null;
		HttpClient cliente = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost(BASE_PATH + "getActualizaciones/format/json");
        List<NameValuePair> parametros = new ArrayList<NameValuePair>();
        parametros.add(new BasicNameValuePair("idDependencia", Double.toString(idDependencia) ));	
        try{
        	httpPost.setEntity(new UrlEncodedFormEntity(parametros, HTTP.UTF_8));
        	HttpResponse resp = cliente.execute(httpPost);
        	HttpEntity ent = resp.getEntity();
        	String respuesta = EntityUtils.toString(ent, HTTP.UTF_8);
        	if(respuesta.length() > 0){
            	Gson gson = new Gson();
            	actualizacion = gson.fromJson(respuesta, Actualizacion.class);        		
        	}
        }catch(Exception e){
        	e.printStackTrace();
        }	              
		
		return actualizacion;   		
	}	
	
	
	/**
	 * Funci�n que descarga y almacena los avisos del sistema de avisos de una dependencia,
	 * la funci�n espera un JSON de Avisos {nombre, descripcion, fechaInicio, fechaFin, lugar}
	 * del servicio web de tipo GET del sistema de avisos de la dependencia asociada.
	 * @return TRUE si se realiz� correctamente, FALSE de lo contrario
	 */
	public boolean actualizarDatosAvisos(){
		avisoController = new AvisoController(context);
		avisoController.eliminarDatosAviso();
		List<Aviso> avisos = null;
		boolean status = false;
		HttpClient cliente = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(BASE_PATH_SERVIDOR_AVISOS + "getAvisos");	
        try{
        	HttpResponse resp = cliente.execute(httpGet);
        	HttpEntity ent = resp.getEntity();
        	String respuesta = EntityUtils.toString(ent, HTTP.UTF_8);
        	if(respuesta.length() > 0){
            	Gson gson = new Gson();
            	Type listType = new TypeToken<List<Aviso>>(){}.getType();
            	avisos = gson.fromJson(respuesta, listType);
            	for(Aviso aviso : avisos){
            		status = avisoController.guardar(aviso);
            	}        		
        	}
        }catch(Exception e){
        	e.printStackTrace();
        }	              
		return status;           
	}			
	
}
