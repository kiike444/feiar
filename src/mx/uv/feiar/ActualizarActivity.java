package mx.uv.feiar;

import mx.uv.feiar.controller.ActualizacionController;
import mx.uv.feiar.controller.DependenciaController;
import mx.uv.feiar.descargadatos.DescargaDeDatos;
import mx.uv.feiar.model.Actualizacion;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


public class ActualizarActivity extends ActionBarActivity{
	private DescargaDeDatos descargaDatos = new DescargaDeDatos(ActualizarActivity.this);
	private DependenciaController dependenciaController;
	private ActualizacionController actualizacionController;
	private ProgressBar progressBar;
	private TextView tvEstado;
	private TextView tvDependenciaEncontrada;
	private LocationManager locManager;
	private LocationListener locListener;
	private boolean obtenerCoordenada = false;
	private boolean hiceClic = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_actualizar);
		
		progressBar = (ProgressBar)findViewById(R.id.progressBar);
		tvEstado = (TextView)findViewById(R.id.tvEstado);
		tvDependenciaEncontrada = (TextView)findViewById(R.id.tvDependenciaEncontrada);
		
		locManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);			
		
		locListener = new LocationListener() {
			 
		    public void onLocationChanged(Location location) {
		    	
		    	 if(location != null && obtenerCoordenada)
		    	    {
		    		    obtenerCoordenada = false;
		    	        double latitud = location.getLatitude();
		    	        double longitud = location.getLongitude();
		    	        Actualizar actualizar = new Actualizar();
		    	        actualizar.execute(latitud, longitud);
		    	    }
		    }
		 
		    public void onProviderDisabled(String provider){}
		 
		    public void onProviderEnabled(String provider){}
		 
		    public void onStatusChanged(String provider, int status, Bundle extras){}
		};
		
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
		    case android.R.id.home:
		        NavUtils.navigateUpFromSameTask(this);
		        return true;
		    }
		return super.onOptionsItemSelected(item);
	}	
	
	public void descargarDatosClic(View v){	
		if(NetworkStatus.getInstance(this).isOnline()){
			if(!hiceClic){
				progressBar.setProgress(0);
				hiceClic = true;
				obtenerCoordenada = true;
				tvEstado.setText("Obteniendo posici�n actual");			
				progressBar.setVisibility(View.VISIBLE);
				locManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, locListener);
				locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 800, 0, locListener);
			}else{
	            Toast.makeText(this, "La descarga de datos ya est� en proceso",Toast.LENGTH_LONG).show();
			}			
		}else{
			Toast.makeText(this, "No hay conexi�n a internet",Toast.LENGTH_LONG).show();
		}
	}
	

	private class Actualizar extends AsyncTask<Double, Integer, Boolean> {

		@Override
		protected Boolean doInBackground(Double... params) {
			double latitud = params[0];
			double longitud = params[1];
			dependenciaController = new DependenciaController(ActualizarActivity.this);
			actualizacionController = new ActualizacionController(ActualizarActivity.this);
			publishProgress(5);
			int idDependencia =  descargaDatos.comprobarDependenciaEnUbicacion(latitud, longitud);

	        	if(idDependencia > 0){
	        		publishProgress(10);
	        		//Se encontr� una dependencia
	        		int idDependenciActual = dependenciaController.getIdDependenciaAsociada();
	        		
	        		if(idDependenciActual == idDependencia){
	        			//Ya tengo datos de esa dependencia
	        			boolean actualizado = false;
	        			Actualizacion actualizacion = descargaDatos.obtenerControlActualizaciones(idDependenciActual);
	        			if(actualizacion != null){

	        				if(!actualizacionController.estaActualizadoDependencia( actualizacion.getDependencia() )){
	        					descargaDatos.actualizarDatosDependencia(idDependencia);
	    	        			publishProgress(20);
	    	        			actualizado=true;
	        				}	        				
	        				
	        				
	        				if(!actualizacionController.estaActualizadoEspacioDeInteres( actualizacion.getEspaciosDeInteres() )){
	    	        			descargaDatos.actualizarDatosEspaciosDeInteres(idDependencia);
	    	        			publishProgress(30);
	    	        			actualizado=true;
	        				}
	        				
	        				if(!actualizacionController.estaActualizadoServicios( actualizacion.getServicios() )){
	        					descargaDatos.actualizarDatosServicios(idDependencia);
	    	        			publishProgress(40);
	    	        			actualizado=true;
	        				}
	        				
	        				if(!actualizacionController.estaActualizadoResponsable( actualizacion.getResponsables() )){
	    	        			descargaDatos.actualizarDatosResponsables(idDependencia);
	    	        			publishProgress(50);
	    	        			actualizado=true;
	        				}	    
	        				
	        				if(!actualizacionController.estaActualizadoEventos( actualizacion.getEventos() )){
	    	        			descargaDatos.actualizarDatosEventosInternos(idDependencia);
	    	        			publishProgress(60);
	    	        			descargaDatos.actualizarDatosEventosExternos(idDependencia);
	    	        			publishProgress(70);
	    	        			actualizado=true;
	        				}	 
	        				
	        				if(!actualizacionController.estaActualizadoSenialamientos( actualizacion.getSenialamientos() )){
	    	        			descargaDatos.actualizarDatosIndicaciones(idDependencia);
	    	        			publishProgress(80);
	    	        			actualizado=true;
	        				}	  	        				
	        				
	        				if(actualizado){
			        			descargaDatos.actualizarControlActualizaciones(idDependencia);		
	        				}
	        				        		
		        			//Descargar los avisos de la dependencia si cuanta con el sistema de avisos
		        			String url = dependenciaController.getUrlServidorAvisosDependenciaAsoaciada();
		        			if(url != null)
		        			if(!url.equals("")){
		        				descargaDatos.actualizarDatosAvisos();
		        				publishProgress(90);
		        			}	        				
	        				
	        				publishProgress(100);	
	        			}
	        			
	        		}else{
	        			//No se tienen datos de la dependencia, por lo tanto se descarga toda la informaci�n
	        			descargaDatos.actualizarDatosDependencia(idDependencia);
	        			publishProgress(20);        						
	        			descargaDatos.actualizarDatosEspaciosDeInteres(idDependencia);
	        			publishProgress(30);    			
	        			descargaDatos.actualizarDatosServicios(idDependencia);
	        			publishProgress(40);        			
	        			descargaDatos.actualizarDatosResponsables(idDependencia);
	        			publishProgress(50);	
	        			descargaDatos.actualizarDatosEventosInternos(idDependencia);
	        			publishProgress(60);
	        			descargaDatos.actualizarDatosEventosExternos(idDependencia);
	        			publishProgress(70);
	        			descargaDatos.actualizarDatosIndicaciones(idDependencia);
	        			publishProgress(80);
	        			descargaDatos.actualizarControlActualizaciones(idDependencia);		
	        			
	        			//Descargar los avisos de la dependencia si cuanta con el sistema de avisos
	        			String url = dependenciaController.getUrlServidorAvisosDependenciaAsoaciada();
	        			if(url != null)
	        			if(!url.equals("")){
	        				descargaDatos.actualizarDatosAvisos();
	        				publishProgress(90);
	        			}
	        			
	        			publishProgress(100);
	        		}
	        	}else{
	        		if(idDependencia == -1){
	        			publishProgress(-1);
	        		}else{
	        			publishProgress(-2);
	        		}
	        		
	        	}
			return true;
		}
		
	    @Override
	    protected void onProgressUpdate(Integer... values) {
	        int progreso = values[0].intValue();

	        if(progreso == -2){
	        	tvEstado.setText("No es posible conectarse al servidor");
	        	progressBar.setProgress(0);
	        }	        
	        
	        if(progreso == -1){
	        	tvEstado.setText("No hay una dependencia registrada en tu ubicaci�n");
	        }
	        
	        if(progreso == 5){
	        	tvEstado.setText("Buscando dependencia en tu ubicaci�n");
	        }	        
	        
	        if(progreso == 10){
	        	tvEstado.setText("Dependencia encontrada");
	        	tvDependenciaEncontrada.setText(descargaDatos.getNombreDependencia());
	        }

	        if(progreso == 20){
	        	tvEstado.setText("Obteniendo datos de dependencia");
	        }
	        if(progreso == 30){
	        	tvEstado.setText("Obteniendo espacios de inter�s");
	        }
	        if(progreso == 40){
	        	tvEstado.setText("Obteniendo servicios");
	        }
	        if(progreso == 50){
	        	tvEstado.setText("Obteniendo responsables de servicios");
	        }
	        if(progreso == 60){
	        	tvEstado.setText("Obteniendo eventos");
	        }
	        if(progreso == 80){
	        	tvEstado.setText("Obteniendo se�alamientos");
	        }
	        if(progreso == 90){
	        	tvEstado.setText("Obteniendo avisos");
	        }	
	        if(progreso == 100){
	        	tvEstado.setText("Descarga de datos finalizada");
	        }		        
	        
	        if(progreso > 1 && progreso <= 100){
	        	progressBar.setProgress(progreso);
	        }
	        
	        
	        //Desactivamos las peticiones de ubicaci�n
	        locManager.removeUpdates(locListener);
	    }		
		
	    @Override
	    protected void onPreExecute() {
	    	progressBar.setMax(100);
	    	progressBar.setProgress(0);
	    }		
		
	    @Override
	    protected void onPostExecute(Boolean result) {
	    	hiceClic = false;      
			actualizacionController.cerrarConexion();
	    }	    
	    
	}
}
