package mx.uv.feiar;

import java.util.List;

import mx.uv.feiar.controller.EspacioDeInteresController;
import mx.uv.feiar.controller.EventoController;
import mx.uv.feiar.model.Evento;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class EventosAdapter extends ArrayAdapter<Object>{
	private Context context;
	
	public EventosAdapter(Context context, List<Object> elementos){
		super(context, R.layout.elemento_lista, elementos);
		this.context = context;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View elemento = inflater.inflate(R.layout.elemento_lista, null);		
		if(getItem(position) instanceof Evento){
			TextView tvNombre = (TextView)elemento.findViewById(R.id.tvElementoListaNombre);
			TextView tvInicio = (TextView)elemento.findViewById(R.id.tvElementoListaInicio);
			TextView tvFin = (TextView)elemento.findViewById(R.id.tvElementoListaFin);
			TextView tvLugar = (TextView)elemento.findViewById(R.id.tvElementoListaLugar);
			TextView tvDescripcion = (TextView)elemento.findViewById(R.id.tvElementoListaDescripcion);
			Evento evento = (Evento) getItem(position);
			tvNombre.setText(evento.getNombre());
			
			String fecha = evento.getFechaInicio().split(" ")[0];
			String hora = evento.getFechaInicio().split(" ")[1];
			hora = hora.substring(0, 5);
			tvInicio.setText(fecha + " a las " + hora);

			fecha = evento.getFechaFin().split(" ")[0];
			hora = evento.getFechaFin().split(" ")[1];
			hora = hora.substring(0, 5);			
			tvFin.setText(fecha + " a las " + hora);
			
			tvDescripcion.setText(evento.getDescripcion());
			if(evento.getLugar() != null){
				tvLugar.setText(evento.getLugar());
			}else{
				EspacioDeInteresController controller = new EspacioDeInteresController(context);
				String lugar = controller.getNombreEspacioDeInteresPorId(evento.getIdEspacioDeInteres());
				tvLugar.setText(lugar);
			}
		}else{
			
		}
		
		return elemento;
	}
	
}
